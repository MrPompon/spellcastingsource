﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class UIEquipmentSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    // Use this for initialization
    public Equipment.EquipmentType UISlotType;
    public bool isLeftEquip;
    Character m_currentChar;
    CharacterEquipment m_currentCharEquip;
    ItemOnCursorHandler itemOnCursorHandler;
    public DisplayItem m_display;
    public Equipment m_equipData;
    bool highlighted = false;
    void Start()
    {
        itemOnCursorHandler = GameObject.FindGameObjectWithTag("GameManager").GetComponent<ItemOnCursorHandler>();
        m_currentChar = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        m_currentCharEquip = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterEquipment>();
        m_display = GameObject.FindGameObjectWithTag("DisplayItemPanel").GetComponent<DisplayItem>();
    }
    public void OnPointerEnter(PointerEventData m_data)
    {
        highlighted = true;
    }
    public void OnPointerExit(PointerEventData p_data)
    {
        highlighted = false;
    }

    void Update()
    {
        if (highlighted)
        {
            if (Input.GetMouseButtonUp(1))
            {
                Equipment tempEquip = itemOnCursorHandler.GetEquipment();
                if (tempEquip != null)
                {
                    InteractWithSlot(itemOnCursorHandler.GetEquipment());
                    Debug.Log("Do stuff");
                }
                else
                {
                    Debug.Log("No Item Assigned to ItemOnCursorHandler");
                }
            }
        }
    }
    public void InteractWithSlot()
    {
        Debug.Log("Beep Boop InventorySlot");
    }
    public void InteractWithSlot(Equipment p_equipment)
    {
        CheckEquipmentType(ref p_equipment);
    }
    public void CheckEquipmentType(ref Equipment p_equipMent)
    {
        Equipment.EquipmentType equipmentType = p_equipMent.m_equipType;

        if (UISlotType == equipmentType)
        {
            switch (equipmentType)
            {
                case EquipmentBase.EquipmentType.TYPE_BOOTS:
                    {
                        if (isLeftEquip)
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_boot_l, ref p_equipMent);
                        }
                        else
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_boot_r, ref p_equipMent);
                        }
                        break;
                    }

                case EquipmentBase.EquipmentType.TYPE_CHEST:
                    {
                        ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_chest, ref p_equipMent);
                        break;
                    }


                case EquipmentBase.EquipmentType.TYPE_HANDS:
                    {
                        if (isLeftEquip)
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_glove_l, ref p_equipMent);
                        }
                        else
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_glove_r, ref p_equipMent);
                        }
                        break;
                    }

                case EquipmentBase.EquipmentType.TYPE_HELMET:
                    {
                        ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_helm, ref p_equipMent);
                        Debug.Log("ReplaceHelmet");
                        break;
                    }


                case EquipmentBase.EquipmentType.TYPE_LEGS:
                    {
                        if (isLeftEquip)
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_leg_l, ref p_equipMent);
                        }
                        else
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_leg_r, ref p_equipMent);
                        }
                        break;
                    }

                case EquipmentBase.EquipmentType.TYPE_NECK:
                    {
                        ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_neck, ref p_equipMent);
                        break;
                    }


                case EquipmentBase.EquipmentType.TYPE_RING:
                    {
                        if (isLeftEquip)
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_ring_l, ref p_equipMent);
                        }
                        else
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_ring_r, ref p_equipMent);

                        }
                        break;
                    }


                case EquipmentBase.EquipmentType.TYPE_WAIST:
                    {
                        ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_waist, ref p_equipMent);
                        break;
                    }


                case EquipmentBase.EquipmentType.TYPE_SHOULDERS:
                    {
                        if (isLeftEquip)
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_shoulder_l, ref p_equipMent);
                        }
                        else
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_shoulder_r, ref p_equipMent);
                        }
                        break;
                    }


                case EquipmentBase.EquipmentType.TYPE_WEAPON:
                    {
                        if (isLeftEquip)
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_lh, ref p_equipMent);
                        }
                        else
                        {
                            ReplaceEquipmentWithOther(ref m_currentCharEquip.m_equipped.equipped_rh, ref p_equipMent);
                        }
                        break;
                    }

            }
        }
        else
        {
            Debug.Log("Equipment Type IS NOT the same, try another slot!");
        }
    }
    void ReplaceEquipmentWithOther(ref Equipment p_targetReplaceEquip, ref Equipment newEquip)
    {
        //Debug.Log("BEFORE" + p_targetReplaceEquip.m_equipdata.name + "is the targetreplace" + newEquip.m_equipdata.name + "is the new equip");
        m_currentCharEquip.EquipEquipment(ref p_targetReplaceEquip, ref newEquip);
        //Debug.Log("AFTER"+ p_targetReplaceEquip.m_equipdata.name+"is the targetreplace"+newEquip.m_equipdata.name+"is the new equip");

    }

}
