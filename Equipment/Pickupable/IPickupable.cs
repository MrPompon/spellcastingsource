﻿using UnityEngine;
using System.Collections;

public interface IPickupable{
    void Interact();
    void Pickup();
    void Hover();
    void Despawn();
    void Spawn();
    void TriggerEvent();
}
