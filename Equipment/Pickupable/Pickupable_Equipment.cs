﻿using UnityEngine;
using System.Collections;

public class Pickupable_Equipment : Interactable, IPickupable{

    public MeshRenderer m_renderer;
    public Equipment m_equip;
    public SpellEffects.AfflictionData m_equipData;
    //make equipdata
    //after chest or thing spawned this, call to set this data equal to the generated data
    public override void Start()
    {
        base.Start();
        m_renderer = GetComponent<MeshRenderer>();
        
        //m_renderer.material.shader=Resources.Load<
    }
    public void Pickup()
    {
        m_equip.m_equipdata = m_equipData;
        
        gMF.GetSelectedPlayer().GetComponent<Inventory>().AddEquipmentToInventory(m_equip);
        Despawn();
    }
    public void Despawn()
    {
        Destroy(this.gameObject);
    }
    public void Spawn()
    {

    }
    public void TriggerEvent()
    {

    }
    public void Hover(){

    }
    public override void Interact()
    {
        base.Interact();
        Pickup();
        Despawn();
        //send itemdata to inventory, despawn this
    }
    public override void OnMouseEnter()
    {
        base.OnMouseEnter();
        //Debug.Log("Mouse entered item"+ this.gameObject.name);
    }
    public override void OnMouseOver()
    {
        base.OnMouseOver();
        //Debug.Log("HoveringOver Item" + this.gameObject.name);
    }
    public override void OnMouseExit()
    {
        base.OnMouseExit();
        //Debug.Log("Mouse exited item" + this.gameObject.name);
    }
}
