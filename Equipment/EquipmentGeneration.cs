﻿using UnityEngine;
using System.Collections;

public class EquipmentGeneration : MonoBehaviour
{
    public GameObject pickupableEquipPrefab;
    public SpellEffects.AfflictionData rarityToStatMod;
    public Transform treasureParent;
    #region ItemNameGeneration
    void Start()
    {
        if (treasureParent == null)
        {
            treasureParent = GameObject.FindGameObjectWithTag("TreasureParent").transform;
        }
    }
    public void GenerateName(ref string itemName)
    {
        GeneratePrefix(ref itemName);
        GenerateBinder(ref itemName);
        GenerateSuffix(ref itemName);
    }
    public void GeneratePrefix(ref string itemName)
    {
        PrefixModStart modstart = (PrefixModStart)Random.Range(0, (int)PrefixModStart.Start_Length);
        PrefixModEnd modend = (PrefixModEnd)Random.Range(0, (int)PrefixModEnd.End_Length);
        itemName = modstart.ToString() + modend.ToString();
        //Debug.Log(itemName);
    }

    void GenerateBinder(ref string itemName)
    {
        BinderNames binder = (BinderNames)Random.Range(0, (int)BinderNames.Binder_Length);
        itemName += " " + binder.ToString();
        //Debug.Log(itemName);
    }
    void GenerateSuffix(ref string itemName)
    {
        SuffixNames suffix = (SuffixNames)Random.Range(0, (int)SuffixNames.Suffix_Length);
        itemName += " " + suffix.ToString();
        //Debug.Log(itemName);
    }

    enum PrefixModStart
    {
        Dark,
        Light,
        Flame,
        Ice,
        Out,
        In,
        Deve,
        Blast,
        Start_Length
    }
    enum PrefixModEnd //decapitelize
    {
        scorn,
        crown,
        stool,
        station,
        slayer,
        grudger,
        End_Length
    }

    enum BinderNames
    {
        of,
        With,
        Against,
        Without,
        With_a_little_bit_of,
        Binder_Length
    }
    enum SuffixNames
    {
        Thunder,
        Lightning,
        Destruction,
        Mana,
        Nothing,
        Suffix_Length
    }
    #endregion
    #region ItemStatGeneration
    public void GenerateItemMods(ref SpellEffects.AfflictionData p_equipData,float p_rarity,float p_modScale) //note to self: it wouldve been smart to not have double struct** but instead, struct and disctionaries i guess?
    {
        int amountOfSpellModStructs = 8;
        int rndModStruct = Random.Range(1, amountOfSpellModStructs);
        //Debug.Log("Item "+ p_equipData.name +" GeneratedEffectMod Number "+rndModStruct); 
        switch (rndModStruct)
        {
            #region ManaMods
            case 1://manamods
                int manaModsAmount = 2;
                int rndManastruct = Random.Range(1, manaModsAmount);
                switch (rndManastruct)
                {
                    case 1://manareg
                        p_equipData.manaMods.sm_manaRegMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.manaMods.sm_manaRegMod);
                        break;
                    case 2://manamax
                        p_equipData.manaMods.sm_manaMaxMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.manaMods.sm_manaMaxMod);
                        break;
                }
                break;
            #endregion
            #region HealthMods
            case 2://healthmods
                int healthModsAmount = 2;
                int rndHealthstruct = Random.Range(1, healthModsAmount);
                switch (rndHealthstruct)
                {
                    case 1:  //healthreg
                        p_equipData.healthMods.sm_hpRegMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.healthMods.sm_hpRegMod);
                        break;
                    case 2://healthmax
                        p_equipData.healthMods.sm_hpMaxMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.healthMods.sm_hpMaxMod);
                        break;
                }
                break;
            #endregion
            #region ResistanceMods
            case 3:
                int resistanceModsAmount = 8;
                int rndResistanceStruct = Random.Range(1, resistanceModsAmount);
                switch (rndResistanceStruct)
                {
                    case 1://allres 
                        p_equipData.resistanceMods.sm_allResistanceMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.resistanceMods.sm_allResistanceMod);
                        break;
                    case 2://lightning
                        p_equipData.resistanceMods.sm_lightningResistanceMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.resistanceMods.sm_lightningResistanceMod);
                        break;
                    case 3://fire
                        p_equipData.resistanceMods.sm_fireResistanceMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.resistanceMods.sm_fireResistanceMod);
                        break;
                    case 4://ice 
                        p_equipData.resistanceMods.sm_iceResistanceMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.resistanceMods.sm_iceResistanceMod);
                        break;
                    case 5://water
                        p_equipData.resistanceMods.sm_waterResistanceMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.resistanceMods.sm_waterResistanceMod);
                        break;
                    case 6://venom
                        p_equipData.resistanceMods.sm_venomResistanceMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.resistanceMods.sm_venomResistanceMod);
                        break;
                    case 7://darkness
                        p_equipData.resistanceMods.sm_darknessResistanceMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.resistanceMods.sm_darknessResistanceMod);
                        break;
                    case 8://holy
                        p_equipData.resistanceMods.sm_holyResistanceMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.resistanceMods.sm_holyResistanceMod);
                        break;
                }
                break;
            #endregion
            #region ArmorMods
            case 4:
                int armorModsAmount = 2;
                int rndArmorstruct = Random.Range(1, armorModsAmount);
                switch (rndArmorstruct)
                {
                    case 1://armorMod
                        p_equipData.armorMods.sm_armorMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.armorMods.sm_armorMod);
                        break;
                    case 2://armorMultiplier
                        p_equipData.armorMods.sm_armorMultiplier += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.armorMods.sm_armorMultiplier);
                        break;
                }
                break;
            #endregion
            #region VelocityMods
            case 5://speedMod 
                p_equipData.velocityMods.sm_speedMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.velocityMods.sm_speedMod);
                break;
            #endregion
            #region DamageAmpMods
            case 6://damageamp
                p_equipData.damageAmpMods.sm_damageDealtMultiplier += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.damageAmpMods.sm_damageDealtMultiplier);
                break;
            #endregion
            #region RecievedDamageAmpMods
            case 7://recieveddmgAmp
                p_equipData.recievedDamageAmpMods.sm_damageRecievedMultiplier += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.recievedDamageAmpMods.sm_damageRecievedMultiplier);
                break;
            #endregion
            #region OffensiveMods
            case 8:
                int offesiveModsAmount = 2;
                int rndOffensiveModsstruct = Random.Range(1, offesiveModsAmount);
                switch (rndOffensiveModsstruct)
                {
                    case 1://attackspeed
                        p_equipData.offensiveMods.sm_attackSpeedMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.offensiveMods.sm_attackSpeedMod);
                        break;
                    case 2://castspeed
                        p_equipData.offensiveMods.sm_castSpeedMod += RarityModscaleTimesModifierValue(p_rarity, p_modScale, rarityToStatMod.offensiveMods.sm_castSpeedMod);
                        break;
                }
                break;
            #endregion       
        }
    }
    float RarityModscaleTimesModifierValue(float p_baseRarity,float p_modscale, float p_effectValueModifier)
    {
        if (p_effectValueModifier != 0)
        {
            return (p_baseRarity *p_modscale)/2 * p_effectValueModifier;
        }
        Debug.Log("EffectValueModifier is 0, returning (baserarity*modscale)/2");
        return (p_baseRarity * p_modscale) / 2;
    }
    enum BaseStatMods
    {

    }
    enum MultiplierMods
    {
    }
    #endregion
    public void DecideItemType(ref Equipment.EquipmentType equip, int rnd)
    {
        
        switch (rnd)
        {
            case 0:
                equip= EquipmentBase.EquipmentType.TYPE_BOOTS;
                break;
            case 1: equip= EquipmentBase.EquipmentType.TYPE_CHEST;
                break;
            case 2: equip= EquipmentBase.EquipmentType.TYPE_HANDS;
                break;
            case 3: equip= EquipmentBase.EquipmentType.TYPE_HELMET;
                break;
            case 4: equip= EquipmentBase.EquipmentType.TYPE_LEGS;
                break;
            case 5: equip= EquipmentBase.EquipmentType.TYPE_NECK;
                break;
            case 6: equip= EquipmentBase.EquipmentType.TYPE_RING;
                break;
            case 7: equip= EquipmentBase.EquipmentType.TYPE_WAIST;
                break;
            case 8: equip = EquipmentBase.EquipmentType.TYPE_WEAPON;
                break;
            case 9: equip = EquipmentBase.EquipmentType.TYPE_SHOULDERS;
                break;
        }
       
       // Debug.Log(equip.ToString());
    }
    /// <summary>
    /// Not tested yet!(default search G/SFX/+param)
    /// </summary>
    /// <param name="p_audioName"></param>
    /// <returns></returns>
    AudioClip LoadAudioFromResources(string p_audioName)
    {
        AudioClip tempClip = Resources.Load<AudioClip>("G/SFX/" + p_audioName) as AudioClip;
        if (tempClip == null)
        {
            Debug.LogError("Could not find " + "G/SFX/" + p_audioName);
        }
        return tempClip;
    }
    Sprite LoadSpriteFromResources(string p_spriteName)
    {
        Sprite tempSprite = Resources.Load<Sprite>("G/SpellCasting/Sprites/UI/EquipmentIcons" + p_spriteName) as Sprite;
        if (tempSprite == null)
        {
            Debug.LogError("Could not find " + "G/SFX/" + p_spriteName);
        }
        return tempSprite;
    }
}
