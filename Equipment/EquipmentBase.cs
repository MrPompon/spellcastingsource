﻿using UnityEngine;
using System.Collections;
using System.IO;
[SerializeField]
[System.Serializable]
public class EquipmentBase{

	// Use this for initialization
    public EquipmentType m_equipType;

    //public Mesh m_equipModel;
    public SpellEffects.AfflictionData m_equipdata;
    public float m_baseDef;
    public float m_baseDefRate;
    public float m_magicRes;
    public float m_weight;
    //currently not doing anything with basestats!(8/11/16)
    [System.Serializable]
    public enum EquipmentType
    {
        TYPE_HELMET,
        TYPE_CHEST,
        TYPE_WAIST,
        TYPE_HANDS,
        TYPE_RING,
        TYPE_NECK,
        TYPE_LEGS,
        TYPE_BOOTS,
        TYPE_SHOULDERS,
        TYPE_WEAPON,
        TYPE_AMOUNT,
    }
}
