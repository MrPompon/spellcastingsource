﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
public class CharacterStatUITooltip : MonoBehaviour
{

    // Use this for initialization

    [SerializeField]
    private bool showLifebar, showManabar;
    public Vector2 windowOffset;
    public float barWidth, barHeight;
    [SerializeField]
    Vector2 lifebarPos, manabarPos, windowBackdropPos;
    [SerializeField]
    private Vector2 windowSize;
    private CharacterResourceManager m_resourceManager;
    private Texture2D lifebarTexture, manabarTexture, backgroundTexture, windowTexture;
    public Image lifeBarFill, lifeBarLip, manaBarFill, manaBarLip;
    //new stuff
    //public int amountOfGUIBars;
    //public string[] gUIBarsNames;
    //public Vector2[] gUIBarsPositions;
    //public UnityEvent[] gUIBarCurrentValue;
    //public UnityEvent[] gUIBarMaximumValue;
    public Dictionary<string, GUIBar> gUIBars = new Dictionary<string, GUIBar>();
    void Awake()
    {
        //for (int i = 0; i < amountOfGUIBars;i++ )
        //{
        //    gUIBars.Add(gUIBarsNames[i],new GUIBar(lifebarTexture,gUIBarsPositions[i],gUIBarsNames[i],gUIBarCurrentValue[i],gUIBarMaximumValue[i]));
        //}
        m_resourceManager = GetComponent<CharacterResourceManager>();
        lifebarTexture = Resources.Load("G/Images/GUI/GUILifebar") as Texture2D;
        manabarTexture = Resources.Load("G/Images/GUI/GUIManabar") as Texture2D;
        backgroundTexture = Resources.Load("G/Images/GUI/GUIBackground") as Texture2D;
        windowTexture = Resources.Load("G/Images/GUI/GUIBackdrop") as Texture2D; //background texture atm
    }
    void LateUpdate()
    {
        float maximumHealth = m_resourceManager.m_resourcesBase.maxHealth;
        float currentHealth = m_resourceManager.m_resourcesBase.currentHealth;
        float currentMana = m_resourceManager.m_resourcesBase.currentMana;
        float maximumMana=m_resourceManager.m_resourcesBase.maxMana;
        if (lifeBarLip != null)
        {
            lifeBarLip.fillAmount = (currentHealth / maximumHealth) + 0.02f;
            lifeBarFill.fillAmount = (currentHealth / maximumHealth);
        }
        if (manaBarFill != null)
        {
            manaBarFill.fillAmount = (currentMana / maximumMana);
            manaBarLip.fillAmount = (currentMana / maximumMana) + 0.02f;
        }


    }
    //void OnGUI()
    //{
    //    Vector3 screenPos = Camera.main.WorldToScreenPoint(this.transform.position);
    //    if (showLifebar || showManabar)
    //    {
    //        GUI.DrawTexture(new Rect(screenPos.x - ((windowBackdropPos.x + windowOffset.x) / 2.0f), Screen.height - screenPos.y - ((windowBackdropPos.y + windowOffset.y) / 2.0f), windowSize.x, windowSize.y), windowTexture, ScaleMode.StretchToFill);
    //    }
    //    if (showLifebar)
    //    {
    //        //TEST FOR NOW WITH 30

    //        float maximumHealth = m_resourceManager.m_resourcesBase.maxHealth;
    //           float currentHealth = m_resourceManager.m_resourcesBase.currentHealth;
    //        //background
    //        GUI.DrawTexture(new Rect(screenPos.x - ((lifebarPos.x + windowOffset.x) / 2.0f), Screen.height - screenPos.y - ((lifebarPos.y + windowOffset.y) / 2.0f), barWidth, barHeight), backgroundTexture, ScaleMode.StretchToFill);
    //        //lifebar
    //        GUI.DrawTexture(new Rect(screenPos.x - ((lifebarPos.x + windowOffset.x) / 2.0f), Screen.height - screenPos.y - ((lifebarPos.y + windowOffset.y) / 2.0f), barWidth * (currentHealth / maximumHealth)
    //        , barHeight), lifebarTexture, ScaleMode.StretchToFill);
    //    }
    //    if (showManabar)
    //    {
    //        float currentMana=m_resourceManager.m_resourcesBase.currentMana;
    //        float maximumMana=m_resourceManager.m_resourcesBase.maxMana;
    //        //background
    //        GUI.DrawTexture(new Rect(screenPos.x - ((manabarPos.x + windowOffset.x) / 2.0f), Screen.height - screenPos.y - ((manabarPos.y + windowOffset.y) / 2.0f), barWidth, barHeight), backgroundTexture, ScaleMode.StretchToFill);
    //        //manabar
    //        GUI.DrawTexture(new Rect(screenPos.x - ((manabarPos.x + windowOffset.x) / 2.0f), Screen.height - screenPos.y - ((manabarPos.y + windowOffset.y) / 2.0f), barWidth*(currentMana /maximumMana), barHeight), manabarTexture, ScaleMode.StretchToFill);
    //    }
    //}
    private static Texture2D GetTextureFromSprite(Sprite sprite) //http://answers.unity3d.com/questions/651984/convert-sprite-image-to-texture.html
    {
        if (sprite.rect.width != sprite.texture.width)
        {
            Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            Color[] newColors = sprite.texture.GetPixels((int)sprite.textureRect.x,
                                                         (int)sprite.textureRect.y,
                                                         (int)sprite.textureRect.width,
                                                         (int)sprite.textureRect.height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }
        else
            return sprite.texture;
    }
}
