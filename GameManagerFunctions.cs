﻿using UnityEngine;
using System.Collections;

public class GameManagerFunctions : MonoBehaviour
{
    public LayerMask obstacleLayer;
    private Transform selectedPlayer;
    public float renderReflectionIntensity=1;
    public float renderAmbientIntensity=1;
	// Use this for initialization
    void Awake()
    {
        Cursor.lockState = CursorLockMode.Confined;
        TextPopUpController.Initialize();
    }
    //void Update()
    //{
    //    if (Time.frameCount % 30 == 0)
    //    {
    //        System.GC.Collect();
    //    }
    //}
    void Start () {
        selectedPlayer = GameObject.FindGameObjectWithTag("Player").transform;
        //mouseSetup
        Cursor.lockState = CursorLockMode.Confined;
        //Cursor.visible=false;
        //graphics settings
        RenderSettings.ambientIntensity = renderAmbientIntensity;
        RenderSettings.reflectionIntensity = renderReflectionIntensity;
        //QualitySettings.vSyncCount = 0;
	}
    public Transform GetSelectedPlayer(){
        return selectedPlayer;
    }
    public bool IsPlayerInSight(Transform from, float range)
    {
        RaycastHit hit;
        if (Physics.Linecast(from.position, selectedPlayer.position, out hit, obstacleLayer))
        {
            return false;
        }
        return true;
    }
    public bool IsPlayerDistanceCloserThen(Transform from, float distance) 
    {
        if (Vector3.Distance(from.position, selectedPlayer.position) < distance)
        {
            return true;
        }
        return false;
    }
}
