﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class AfflictionFunctions{

    public SpellEffects.AfflictionData.HealthMods CreateHealthMod(float p_hpRegMod, float p_hpMaxMod)
    {
        SpellEffects.AfflictionData.HealthMods t_healthMods = new SpellEffects.AfflictionData.HealthMods();
        t_healthMods.sm_hpRegMod = p_hpRegMod;
        t_healthMods.sm_hpMaxMod = p_hpMaxMod;
        return t_healthMods;
    }
    public SpellEffects.AfflictionData.ResistanceMods CreateResistanceMod(float p_allResMod, float p_ligthningResMod, float p_fireResMod, float p_iceResMod, float p_waterResMod, float p_venomResMod, float p_darknessResMod, float p_holyResMod)
    {
        SpellEffects.AfflictionData.ResistanceMods t_resMods = new SpellEffects.AfflictionData.ResistanceMods();
        t_resMods.sm_allResistanceMod = p_allResMod;
        t_resMods.sm_lightningResistanceMod = p_ligthningResMod;
        t_resMods.sm_fireResistanceMod = p_fireResMod;
        t_resMods.sm_iceResistanceMod = p_iceResMod;
        t_resMods.sm_waterResistanceMod = p_waterResMod;
        t_resMods.sm_venomResistanceMod = p_venomResMod;
        t_resMods.sm_darknessResistanceMod = p_darknessResMod;
        t_resMods.sm_holyResistanceMod = p_holyResMod;
        return t_resMods;
    }
    public SpellEffects.AfflictionData.ArmorMods CreateArmorMod(float p_armorMod, float p_armorMultiplier)
    {
        SpellEffects.AfflictionData.ArmorMods t_armorMods = new SpellEffects.AfflictionData.ArmorMods();
        t_armorMods.sm_armorMod = p_armorMod;
        t_armorMods.sm_armorMultiplier = p_armorMultiplier;
        return t_armorMods;
    }
    public SpellEffects.AfflictionData.VelocityMods CreateVelocityMod(float p_speedMod)
    {
        SpellEffects.AfflictionData.VelocityMods t_velocityMods = new SpellEffects.AfflictionData.VelocityMods();
        t_velocityMods.sm_speedMod = p_speedMod;
        return t_velocityMods;
    }
    public SpellEffects.AfflictionData.DamageAmplificationMods CreateDamageAmpMod(float p_damageAmpMultiplier)
    {
        SpellEffects.AfflictionData.DamageAmplificationMods t_damageAmpMod = new SpellEffects.AfflictionData.DamageAmplificationMods();
        t_damageAmpMod.sm_damageDealtMultiplier = p_damageAmpMultiplier;
        return t_damageAmpMod;
    }
    public SpellEffects.AfflictionData.RecievedDamageAmplificationMods CreateRecieveDamageAmpMod(float p_damageRecMultiplier)
    {
        SpellEffects.AfflictionData.RecievedDamageAmplificationMods t_recieveDamageAmpMod = new SpellEffects.AfflictionData.RecievedDamageAmplificationMods();
        t_recieveDamageAmpMod.sm_damageRecievedMultiplier = p_damageRecMultiplier;
        return t_recieveDamageAmpMod;
    }
    public SpellEffects.AfflictionData.ManaMods CreateManaMod(float p_manaReg, float p_manaMax)
    {
        SpellEffects.AfflictionData.ManaMods t_manaMod = new SpellEffects.AfflictionData.ManaMods();
        t_manaMod.sm_manaRegMod = p_manaReg;
        t_manaMod.sm_manaMaxMod = p_manaMax;
        return t_manaMod;
    }
    public SpellEffects.AfflictionData.OffensiveMods CreateOffensiveMod(float p_castSpeedMod, float p_attackSpeedMod)
    {
        SpellEffects.AfflictionData.OffensiveMods t_offensiveMod = new AfflictionData.OffensiveMods();
        t_offensiveMod.sm_castSpeedMod = p_castSpeedMod;
        t_offensiveMod.sm_attackSpeedMod = p_attackSpeedMod;
        return t_offensiveMod;
    }
    [System.Serializable]
    public struct AfflictionData //Mod == Modifier
    {
        public string name;
        public float duration;
        public HealthMods healthMods;
        public ResistanceMods resistanceMods;
        public ArmorMods armorMods;
        public VelocityMods velocityMods;
        public DamageAmplificationMods damageAmpMods;
        public RecievedDamageAmplificationMods recievedDamageAmpMods;
        public ManaMods manaMods;
        public OffensiveMods offensiveMods;
        public SpellEffects.SpellSchool.School school;
        //overall
        [System.Serializable]
        public struct ManaMods
        {
            public float sm_manaRegMod;
            public float sm_manaMaxMod;
        }
        [System.Serializable]
        public struct HealthMods
        {
            public float sm_hpRegMod;
            public float sm_hpMaxMod;
        }
        [System.Serializable]
        public struct ResistanceMods
        {
            public float sm_allResistanceMod;
            public float sm_lightningResistanceMod;
            public float sm_fireResistanceMod;
            public float sm_iceResistanceMod;
            public float sm_waterResistanceMod;
            public float sm_venomResistanceMod;
            public float sm_darknessResistanceMod;
            public float sm_holyResistanceMod;
        }
        [System.Serializable]
        public struct ArmorMods
        {
            public float sm_armorMod;
            public float sm_armorMultiplier;
        }
        [System.Serializable]
        public struct VelocityMods
        {
            public float sm_speedMod;
        }
        [System.Serializable]
        public struct DamageAmplificationMods
        {
            public float sm_damageDealtMultiplier; //note to self:: all multipliers default should be 1 and not 0. 
        }
        [System.Serializable]
        public struct RecievedDamageAmplificationMods
        {
            public float sm_damageRecievedMultiplier;
        }
        [System.Serializable]
        public struct OffensiveMods
        {
            public float sm_attackSpeedMod;
            public float sm_castSpeedMod;
        }
    }
}
