﻿using UnityEngine;
using System.Collections;

public class SpellTypeEnums : MonoBehaviour {
    public enum SpellCastingType
    {
        CASTTYPE_DIRECT,
        CASTTYPE_CHOOSEDIRECTION,
        CASTTYPE_CHOOSELOCATION,
    }
    public enum SpellTypeProjectile
    {
        TYPEPROJECTILE_BALL,
        TYPEPROJECTILE_AOE,
        TYPEPROJECTILE_RAY,
    }
    public enum SpellTypeAffected
    {
        TYPETARGET_SINGLE,
        TYPETARGET_MULTIPLE,
        TYPETARGET_AOE,
    }
    public enum SpellProjectileCollisionBehaviour
    {
        COLLISION_EXPLODE,
        COLLISION_BOUNCE,
        COLLISION_PIERCE,
    }
}
