﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class SpellBook : MonoBehaviour{

    private string spellBookName;
    [SerializeField]
    public List<Spell> m_knownSpells;
    void Awake()
    {
        m_knownSpells = new List<Spell>();
    }
    /// <summary>
    /// Returns the Spells the book contains, converted from List to Array.
    /// </summary>
    public string GetSpellBookName()
    {
        return spellBookName;
    }
    public void SetSpellBookName(string p_spellBookName)
    {
        spellBookName = p_spellBookName;
    }
    public void SetSpells(List<Spell> p_spells)
    {
        m_knownSpells = p_spells;
    }
    public Spell[] GetSpells()
    {
        return m_knownSpells.ToArray();
    }
    public void AddSpellToList(Spell p_spell)
    {
            m_knownSpells.Add(p_spell);
            //m_test.Add(p_spell);
            Debug.Log("Adding Spell " + p_spell.m_spellData.m_name + " to the spellbook " + spellBookName);
    }
    public Spell GetSpellByIndex(int p_index)
    {
        return m_knownSpells.ToArray()[p_index];
    }

}
