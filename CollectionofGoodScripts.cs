﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectionofGoodScripts : MonoBehaviour {

    //public delegate IEnumerator WaitForFunction(int p_time);
    
    //RigidBody Manipulation
    public void ApplyDirectionalForceToRigidBody(GameObject p_affected, Vector3 p_dir, float p_force, bool invertDirection)
    {
        Rigidbody t_rigid = p_affected.GetComponent<Rigidbody>();
        if (t_rigid != null)
        {
            if (invertDirection)
            {
                t_rigid.AddForce(-p_dir * p_force);
            }
            else
            {
                t_rigid.AddForce(p_dir * p_force);
            }
        }
        else
        {
            Debug.Log("TargetObject" + p_affected.name + "has no rigidbody");
        }
    }
    //FOWRayCast
    public bool RaycastForOtherWithFOV(GameObject mainFromObject, Transform rayCastFrom, float p_FOV, float p_sigthRange, Collider other, int p_LayerMask, string lookForTag, out Vector3 p_hitPosition)
    {
        p_hitPosition = Vector3.zero;
        if (other.gameObject.CompareTag(lookForTag) && other.gameObject.transform.root != mainFromObject.transform.root)
        {
            Vector3 direction = other.transform.position - rayCastFrom.transform.position;
            float angle = Vector3.Angle(direction, rayCastFrom.transform.forward);

            // If the angle between forward and where the player is, is less than half the angle of view...
            if (angle < p_FOV * 0.5f)
            {
                RaycastHit hit;
                if (Physics.Raycast(rayCastFrom.position + rayCastFrom.up, direction.normalized, out hit, p_sigthRange, p_LayerMask))
                {
                    if (hit.collider.gameObject.CompareTag(lookForTag))
                    {
                        //Debug.Log(hit.collider);
                        p_hitPosition = hit.transform.position;
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public bool RaycastForOtherWithFOV(GameObject mainFromObject, Transform rayCastFrom, float p_FOV, float p_sigthRange, Vector3 position, int p_LayerMask, string lookForTag, out Vector3 p_hitPosition)
    {
        p_hitPosition = Vector3.zero;
        Vector3 direction = position - rayCastFrom.transform.position;
        float angle = Vector3.Angle(direction, rayCastFrom.transform.forward);

        // If the angle between forward and where the player is, is less than half the angle of view...
        if (angle < p_FOV * 0.5f)
        {
            RaycastHit hit;
            if (Physics.Raycast(rayCastFrom.position + rayCastFrom.up, direction.normalized, out hit, p_sigthRange, p_LayerMask))
            {
                if (hit.collider.gameObject.CompareTag(lookForTag) && hit.collider.gameObject.transform.root != mainFromObject.transform.root)
                {
                    p_hitPosition = hit.transform.position;
                    return true;
                }
            }
        }
        return false;
    }
    //MultiCasting
    public static Collider[] BoxCastForObjectsInArea(Vector3 p_orgin, Vector3 boxSize, Vector3 p_direction, LayerMask p_affectedLayer, QueryTriggerInteraction queryInteraction)
    {
        Collider[] hitObjects = Physics.OverlapBox(p_orgin, boxSize, Quaternion.identity, p_affectedLayer, queryInteraction);
        return hitObjects;
    }
    public static Collider[] SphereCastForObjectsWithinRange(Vector3 p_orgin, float p_radius, LayerMask p_affectedLayer, QueryTriggerInteraction p_queryInteraction)
    {
        Collider[] hitObjects = Physics.OverlapSphere(p_orgin, p_radius, p_affectedLayer, p_queryInteraction);
        return hitObjects;
    }
    public static Vector3 GetDirectionBetween(Vector3 orgin, Vector3 target)// target - orgin always reversed.
    {
        Vector3 dir = target - orgin;
        return dir;
    }
    public static Collider[] CheckIfObjectsAreVisibleFromPoint(Collider[] p_objectsInArea, Vector3 p_explosionOrgin, LayerMask p_obstructionLayer)//check if colliders are visible from point
    {
        List<Collider> collList = new List<Collider>();
        for (uint i = 0; i < p_objectsInArea.Length; i++)
        {
            if (Physics.Linecast(p_explosionOrgin, p_objectsInArea[i].transform.position, p_obstructionLayer) == false)
            {
                collList.Add(p_objectsInArea[i]);
            }
        }
        return collList.ToArray();
    }
}
