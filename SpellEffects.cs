﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class SpellEffects: AfflictionFunctions {

    public struct SpellSchool
    {
        public School sm_currentSchool;
        public enum School
        {
            SCHOOL_DESTRUCTION,
            SCHOOL_ALTERATION,
            SCHOOL_RESTORATION,
            SCHOOL_CONJURATION,
        }
    }
  
    public void ApplyDirectionalForce(GameObject p_affected, Vector3 p_dir, float p_force, bool invertForce)
    {
        Rigidbody t_rigid = p_affected.GetComponent<Rigidbody>();
        if (t_rigid!= null)
        {
            if (invertForce)
            {
                t_rigid.AddForce(-p_dir * p_force);
            }
            else
            {
                t_rigid.AddForce(p_dir * p_force);
            }
        }
        else
        {
           // Debug.Log("TargetObject" + p_affected.name + "has no rigidbody");
        }
    }
    //public void RootTransform(Rigidbody p_affected, float duration)
    //{
    //    if (p_affected != null)
    //    {
    //        p_affected.constraints = RigidbodyConstraints.FreezeAll;
    //        StartCoroutine(UnrootTransform(p_affected, duration));
    //    }
    //}
    //private IEnumerator UnrootTransform(Rigidbody p_affected,float p_duration)
    //{
    //    yield return new WaitForSeconds(p_duration);
    //    p_affected.constraints = RigidbodyConstraints.None;
    //}
    public void Teleport(Transform p_affected, Vector3 p_target)
    {
        p_affected.position = p_target;
    }
}
