﻿using UnityEngine;
using System.Collections;

public class AfflictionBase{
    
    public SpellEffects.AfflictionData m_afflictionData;
    public float m_currentDotProgress;
    public float m_dotRate;
    public bool m_willTrigger;
}
