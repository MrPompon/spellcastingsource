﻿using UnityEngine;
using System.Collections;

public class Affliction:AfflictionBehavior{
   
    public AfflictionReciever m_afflictionTarget; //what am i on? target
    public Affliction(SpellEffects.AfflictionData p_afflictionData){
        m_afflictionData = p_afflictionData;
    }
    public void StartAffliction()
    {
        //m_afflictionData = new SpellEffects.AfflictionData();
    }
    public void UpdateAffliction(float updateRate)
    {
        m_afflictionData.duration-=updateRate;
        m_currentDotProgress += updateRate;
    }
}
