﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AfflictionReciever : MonoBehaviour {
    public int currentAfflictionAmount;
    public bool canStackAfflictions;
    public float afflictionUpdateRate = 0.2f;
    private List<Affliction> m_buffs;
    private List<Affliction> m_curses;
    public SpellEffects.AfflictionData m_calculatedAfflictionData;
    private bool startedUpdate=false;
    void Start(){
        m_calculatedAfflictionData = new SpellEffects.AfflictionData();
        m_buffs=new List<Affliction>();
        m_curses=new List<Affliction>();
    }
    void Update()
    {
        if (!startedUpdate)
        {
            StartCoroutine(UpdateAfflictionsWithInterval(afflictionUpdateRate));
            startedUpdate = true;
        }
    }
    IEnumerator UpdateAfflictionsWithInterval(float p_updateRate)
    {
        while (true)
        {
            ManageBuffList(p_updateRate);
            ManageCurseList(p_updateRate);
            //Debug.Log(m_calculatedAfflictionData.armorMods.sm_armorMod + "CURRENT ARMORMOD");
            //Debug.Log(m_calculatedAfflictionData.resistanceMods.sm_lightningResistanceMod + "Current Lightning Res");
            //Debug.Log(m_calculatedAfflictionData.healthMods.sm_hpRegMod);
            yield return new WaitForSeconds(p_updateRate);
        }

    }
    public void AddBuff(Affliction newAffliction)
    {
        if (canStackAfflictions)
        {
            m_buffs.Add(newAffliction);
            PlusAfflictionModsAndStoreInData(ref m_calculatedAfflictionData, newAffliction,false);
            UpdateReciever();
        }
        else if (CheckIfStringDoesNotExistInList(newAffliction.GetName(), m_buffs))
        {
            m_buffs.Add(newAffliction);
            PlusAfflictionModsAndStoreInData(ref m_calculatedAfflictionData, newAffliction,false);
            UpdateReciever();
        }
        else
        {
            newAffliction = null;
        }
    }
    public void AddCurse(Affliction newCurse)
    {
        if (canStackAfflictions)
        {
            m_curses.Add(newCurse);
            PlusAfflictionModsAndStoreInData(ref m_calculatedAfflictionData, newCurse,false);
            UpdateReciever();
        }
        else if (CheckIfStringDoesNotExistInList(newCurse.GetName(), m_curses))
        {
            m_curses.Add(newCurse);
            PlusAfflictionModsAndStoreInData(ref m_calculatedAfflictionData, newCurse,false);
            UpdateReciever();
        }
        else
        {
            newCurse = null;
        }
    }
    void UpdateReciever()
    {
        //ResetAfflictionModsStorageData(ref m_calculatedAfflictionData);
        //ManageBuffList();
        //ManageCurseList();
        CalculateAfflictionAmount();
        //PrintAfflictionData(m_calculatedAfflictionData);
    }
    void UpdateAffliction(Affliction p_afl,float p_updateRate)
    {
        p_afl.UpdateAffliction(p_updateRate);
    }
    void CurseCheckAfflictionDuration(Affliction p_afl)
    {
        if (p_afl.IsDurationOver())
        {
            if (m_curses.Contains(p_afl))
            {
                m_curses.Remove(p_afl);
                SubtractAfflictionModsAndStoreInData(ref m_calculatedAfflictionData, p_afl,true);
                CalculateAfflictionAmount();
            }
            else
            {
                Debug.Log("Couldnt find a CURSE which timer went out, wtf?");
            }
        }
    }
    void BuffCheckAfflictionDuration(Affliction p_afl)
    {
        if (p_afl.IsDurationOver())
        {
            if (m_buffs.Contains(p_afl))
            {
                m_buffs.Remove(p_afl);
                SubtractAfflictionModsAndStoreInData(ref m_calculatedAfflictionData, p_afl,true);
                CalculateAfflictionAmount();
            }
            else
            {
                Debug.Log("Couldnt find a BUFF which timer went out, wtf?");
            }
        }
    }
    void ManageBuffList(float p_updateRate)
    {
        for (int i = m_buffs.Count - 1; i > -1; i--)
        {
            UpdateAffliction(m_buffs[i], p_updateRate);
            BuffCheckAfflictionDuration(m_buffs[i]);
        } 
    }
    void ManageCurseList(float p_updateRate)
    {
        for (int i = m_curses.Count - 1; i > -1; i--)
        {
            UpdateAffliction(m_curses[i], p_updateRate);
            CurseCheckAfflictionDuration(m_curses[i]);
            //PlusAfflictionModsAndStoreInData(ref m_calculatedAfflictionData, afl);
        }
    }
    void CalculateAfflictionAmount()
    {
        currentAfflictionAmount = m_buffs.Count + m_curses.Count;
       // Debug.Log(currentAfflictionAmount);
    }
    void ResetAfflictionModsStorageData(ref SpellEffects.AfflictionData p_storage)
    {

    }
    void AddArmorMods(ref SpellEffects.AfflictionData.ArmorMods p_storage, SpellEffects.AfflictionData.ArmorMods p_affliction, bool p_subtract)
    {
        AddorSubtractFloats(ref p_storage.sm_armorMod,p_affliction.sm_armorMod,p_subtract);
        AddorSubtractFloats(ref p_storage.sm_armorMultiplier, p_affliction.sm_armorMultiplier, p_subtract);
    }
    void AddHealthMods(ref SpellEffects.AfflictionData.HealthMods p_storage, SpellEffects.AfflictionData.HealthMods p_affliction, bool p_subtract)
    {
        AddorSubtractFloats(ref p_storage.sm_hpMaxMod, p_affliction.sm_hpMaxMod, p_subtract);
        AddorSubtractFloats(ref p_storage.sm_hpRegMod, p_affliction.sm_hpRegMod, p_subtract);
    }
    void AddManaMods(ref SpellEffects.AfflictionData.ManaMods p_storage, SpellEffects.AfflictionData.ManaMods p_affliction, bool p_subtract)
    {
        AddorSubtractFloats(ref p_storage.sm_manaMaxMod, p_affliction.sm_manaMaxMod, p_subtract);
        AddorSubtractFloats(ref p_storage.sm_manaRegMod, p_affliction.sm_manaRegMod, p_subtract);
    }
    void AddResistanceMods(ref SpellEffects.AfflictionData.ResistanceMods p_storage, SpellEffects.AfflictionData.ResistanceMods p_affliction, bool p_subtract)
    {
        AddorSubtractFloats(ref p_storage.sm_allResistanceMod, p_affliction.sm_allResistanceMod, p_subtract);
        AddorSubtractFloats(ref p_storage.sm_darknessResistanceMod, p_affliction.sm_darknessResistanceMod, p_subtract);
        AddorSubtractFloats(ref p_storage.sm_fireResistanceMod, p_affliction.sm_fireResistanceMod, p_subtract);
        AddorSubtractFloats(ref p_storage.sm_holyResistanceMod, p_affliction.sm_holyResistanceMod, p_subtract);
        AddorSubtractFloats(ref p_storage.sm_iceResistanceMod, p_affliction.sm_iceResistanceMod, p_subtract);
        AddorSubtractFloats(ref p_storage.sm_lightningResistanceMod, p_affliction.sm_lightningResistanceMod, p_subtract);
        AddorSubtractFloats(ref p_storage.sm_venomResistanceMod, p_affliction.sm_venomResistanceMod, p_subtract);
        AddorSubtractFloats(ref p_storage.sm_waterResistanceMod, p_affliction.sm_waterResistanceMod, p_subtract);
    }
    void AddVelocityMods(ref SpellEffects.AfflictionData.VelocityMods p_storage, SpellEffects.AfflictionData.VelocityMods p_affliction, bool p_subtract)
    {
        p_storage.sm_speedMod += p_affliction.sm_speedMod;
    }
    void AddDamageAmplificationMods(ref SpellEffects.AfflictionData.DamageAmplificationMods p_storage, SpellEffects.AfflictionData.DamageAmplificationMods p_affliction,bool p_subtract)
    {
        AddorSubtractFloats(ref p_storage.sm_damageDealtMultiplier, p_affliction.sm_damageDealtMultiplier, p_subtract);
    }
    void AddRecievedDamageAmplificationMods(ref SpellEffects.AfflictionData.RecievedDamageAmplificationMods p_storage, SpellEffects.AfflictionData.RecievedDamageAmplificationMods p_affliction,bool p_subtract)
    {
        AddorSubtractFloats(ref p_storage.sm_damageRecievedMultiplier, p_affliction.sm_damageRecievedMultiplier, p_subtract);
    }
    void AddOffensiveMods(ref SpellEffects.AfflictionData.OffensiveMods p_storage, SpellEffects.AfflictionData.OffensiveMods p_affliction, bool p_subtract)
    {
        AddorSubtractFloats(ref p_storage.sm_attackSpeedMod, p_affliction.sm_attackSpeedMod, p_subtract);
        AddorSubtractFloats(ref p_storage.sm_castSpeedMod, p_affliction.sm_castSpeedMod, p_subtract);
    }
    
    void AddorSubtractFloats(ref float p_storage, float p_affliction, bool p_subtract)
    {
        if (!p_subtract)
        {
            p_storage += p_affliction;
        }
        else if (p_subtract)
        {
            p_storage -= p_affliction;
        }
    }
    void PlusAfflictionModsAndStoreInData(ref SpellEffects.AfflictionData p_storage, Affliction p_affliction, bool p_subtract)
    {
        AddArmorMods(ref p_storage.armorMods, p_affliction.m_afflictionData.armorMods,p_subtract);
        AddHealthMods(ref p_storage.healthMods,p_affliction.m_afflictionData.healthMods,p_subtract);
        AddManaMods(ref p_storage.manaMods, p_affliction.m_afflictionData.manaMods, p_subtract);
        AddResistanceMods(ref p_storage.resistanceMods, p_affliction.m_afflictionData.resistanceMods, p_subtract);
        AddVelocityMods(ref p_storage.velocityMods, p_affliction.m_afflictionData.velocityMods, p_subtract);
        AddDamageAmplificationMods(ref p_storage.damageAmpMods, p_affliction.m_afflictionData.damageAmpMods, p_subtract);
        AddRecievedDamageAmplificationMods(ref p_storage.recievedDamageAmpMods, p_affliction.m_afflictionData.recievedDamageAmpMods, p_subtract);
        AddOffensiveMods(ref p_storage.offensiveMods, p_affliction.m_afflictionData.offensiveMods, p_subtract);
    }
    void SubtractAfflictionModsAndStoreInData(ref SpellEffects.AfflictionData p_storage, Affliction p_affliction, bool p_subtract) //the only difference is the bool parameter that should be true, easieer on readablity
    {
        AddArmorMods(ref p_storage.armorMods, p_affliction.m_afflictionData.armorMods, p_subtract);
        AddHealthMods(ref p_storage.healthMods, p_affliction.m_afflictionData.healthMods, p_subtract);
        AddManaMods(ref p_storage.manaMods, p_affliction.m_afflictionData.manaMods, p_subtract);
        AddResistanceMods(ref p_storage.resistanceMods, p_affliction.m_afflictionData.resistanceMods, p_subtract);
        AddVelocityMods(ref p_storage.velocityMods, p_affliction.m_afflictionData.velocityMods, p_subtract);
        AddDamageAmplificationMods(ref p_storage.damageAmpMods, p_affliction.m_afflictionData.damageAmpMods, p_subtract);
        AddRecievedDamageAmplificationMods(ref p_storage.recievedDamageAmpMods, p_affliction.m_afflictionData.recievedDamageAmpMods, p_subtract);
        AddOffensiveMods(ref p_storage.offensiveMods, p_affliction.m_afflictionData.offensiveMods, p_subtract);
    }
    void PrintAfflictionData(SpellEffects.AfflictionData p_storage)
    {
        //Debug.Log(p_storage.smc_armorMod);
        //Debug.Log(p_storage.smc_hpMaxMod);
        //Debug.Log(p_storage.smc_hpRegMod);
        //Debug.Log(p_storage.smc_speedMod);
    }
    bool CheckIfStringDoesNotExistInList(string p_toCheckFor,List<Affliction> p_list)
    {
        bool isUnique = true;
        foreach (Affliction afl in p_list)
        {
            if (afl.GetName() == p_toCheckFor)
            {
                isUnique = false;
                return isUnique;
            }
        }
        return isUnique;
    }
}
