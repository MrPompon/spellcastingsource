﻿using UnityEngine;
using System.Collections;

public class AfflictionBehavior : AfflictionBase {
    public bool IsDurationOver()
    {
        if (m_afflictionData.duration <= 0)
        {
            return true;
        }
        return false;
    }
    public void AfflictionEnd()
    {

    }

    public bool GetWillTrigger(float p_dotRate)
    {
        if (m_currentDotProgress >= p_dotRate)
        {
            ResetDotProgress();
            return true;
        }
        return false;
    }
    public void ResetAfflictionDuration()
    {
        m_afflictionData.duration = 0;
    }
    public void ResetDotProgress()
    {
        m_currentDotProgress = 0;
    }
    public void TriggerAffliction()
    {
        //if does special stuff i guess?
    }
    public void TriggerDot()
    {
        m_currentDotProgress = m_dotRate;
    }
    public string GetName()
    {
        return m_afflictionData.name;
    }
 
}
