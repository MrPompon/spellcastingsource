﻿using UnityEngine;
using System.Collections;

public enum SpellState
{
    STATE_WITHHELD,
    STATE_BEINGCHANNELED,
    STATE_INTERRUPTED,
    STATE_SELFCHANNELING,
    STATE_READY,
}
[System.Serializable]
public class Spell : SpellBehaviour
{

    // Use this for initialization
    public enum SpellManaState
    {
        MANA_UNPAID,
        MANA_UPKEEP,
        MANA_PAID,
    }
    public SpellState m_spellState;
    public SpellManaState m_manaState;
    public SpellEffects m_spellEffects = new SpellEffects();
    public GameObject m_originator;
    //public Spell(GameObject originator,
    //    string p_name,
    //    SpellTypeEnums.SpellTypeProjectile p_typeProjectile,
    //    SpellTypeEnums.SpellTypeAffected p_typeAffected,
    //    SpellTypeEnums.SpellProjectileCollisionBehaviour p_collisionBehaviour,
    //    bool p_channelCast,
    //    bool p_canMoveWhileChanneling,
    //    float channelingCastRate,
    //    float p_damage, float p_radius, float p_projectileSpeed, bool p_isHoming, float p_homingTurnRate, int p_bounceAmount, bool p_triggerEffectOnBounce,
    //    bool p_knocksBack, float p_knockbackForce, int p_knockBackDistanceFalloff,
    //    bool p_isTriggeredWithInterval, float p_triggerInterval,
    //    LayerMask p_affectedLayer, LayerMask p_obstacleLayer)
    //{
    //    AtSpellStart();
    //    m_spellCollisionBehavior = p_collisionBehaviour;
    //    m_spellData.bounceData.m_bounceAmount = p_bounceAmount;
    //    m_spellData = originator;
    //    m_spellData.m_name = p_name;
    //    m_spellData.m_typeProjectile = p_typeProjectile;
    //    m_spellData.homingData.m_isHoming = p_isHoming;
    //    m_spellData.homingData.m_homingTurnRate = p_homingTurnRate;
    //    m_spellData.m_typeAffected = p_typeAffected;
    //    m_spellData.channelingData.m_isChannelCast = p_channelCast;
    //    m_spellData.projectileData.m_projectileSpeed = p_projectileSpeed;
    //    m_spellData.globalData.m_damage = p_damage;
    //    m_spellData.aoeData.m_radius = p_radius;

    //    m_spellData.layerData.m_affectedLayer = p_affectedLayer;
    //    m_spellData.layerData.m_obstacleLayer = p_obstacleLayer;

    //    m_spellData.knockBackData.m_afflictsKnockback = p_knocksBack;
    //    m_spellData.knockBackData.m_knockbackForce = p_knockbackForce;
    //    m_spellData.knockBackData.m_knockbackDistanceFalloff = p_knockBackDistanceFalloff;

    //    m_spellData.pulseData.m_IsTriggeredWithInterval = p_isTriggeredWithInterval;
    //    m_spellData.pulseData.m_triggerIntervalRate = p_triggerInterval;

    //}
    public Spell(SpellData p_data)
    {
        //AtSpellStart();
        m_spellData = p_data;
    }
    void OnEnable()
    {
        Debug.Log("I AM PROJECTILE");
    }
    public void OverwriteSpellData(SpellData p_data)
    {
        m_spellData = p_data;
        ResetChannelingData();
        SetSpellStateChanneling();
    }
    public void ResetChannelingData()
    {
        m_spellData.channelingData.m_channelingProgress = 0;
    }
    void SetSpellStateChanneling()
    {
        m_spellState = SpellState.STATE_BEINGCHANNELED;
    }


    // Update is called once per frame



    public void InvokeRay(Vector3 p_orgin, Vector3 p_dir)
    {
        Vector3 t_orgin = p_orgin;
        float t_radius = m_spellData.aoeData.m_radius;
        Collider[] targetsInRayAOE = GlobalRaycast.BoxCastForObjectsInArea(t_orgin, new Vector3(t_radius / 3, t_radius / 3, m_spellData.globalData.m_range), p_dir, m_spellData.layerData.m_affectedLayer);
        Collider[] spellTargets = GlobalRaycast.RefineCollidersInExplosionFOV(targetsInRayAOE, t_orgin, m_spellData.layerData.m_obstacleLayer);
        for (uint i = 0; i < spellTargets.Length; i++)
        {
            Vector3 dir = GlobalRaycast.GetDirectionBetween(p_orgin, spellTargets[i].transform.position);
            float t_distance = Vector3.Distance(spellTargets[i].transform.position, p_orgin);
            for (uint u = 0; u < m_spellData.globalData.m_spawnAmount; u++)
            {
                ExplodeConditionals(spellTargets[i], p_orgin, dir.normalized, t_distance);
            }
        }
    }
    public void InvokeAOESpell(Vector3 p_orgin, GameObject p_explosionEffect)
    {
        //Vector3 targetPosition = new Vector3(m_spellData.positioningData.spellOrginX, m_spellData.positioningData.spellOrginY, m_spellData.positioningData.spellOrginZ);
        CreateExplosionEffect(p_orgin, m_spellData.aoeData.m_radius, 0.15f, p_explosionEffect);
        SoundPlayer.PlayRandomSoundAtPointFromArray(audioData.m_spellTriggerSounds, p_orgin);
        Vector3 t_orgin = p_orgin;
        Collider[] targetsInAOE = GlobalRaycast.SphereCastForObjectsWithinRange(t_orgin, m_spellData.aoeData.m_radius, m_spellData.layerData.m_affectedLayer);
        Collider[] spellTargets = GlobalRaycast.RefineCollidersInExplosionFOV(targetsInAOE, t_orgin, m_spellData.layerData.m_obstacleLayer);
        for (uint i = 0; i < spellTargets.Length; i++)
        {
            Vector3 dir = GlobalRaycast.GetDirectionBetween(p_orgin, spellTargets[i].transform.position);
            float t_distance = Vector3.Distance(spellTargets[i].transform.position, p_orgin);
            for (uint u = 0; u < m_spellData.globalData.m_spawnAmount; u++)
            {
                ExplodeConditionals(spellTargets[i], p_orgin, dir.normalized, t_distance);
            }
        }
        ApplyTerrainEffect(p_orgin, m_spellData.aoeData.m_radius / 10);
    }
    private void ExplodeConditionals(Collider hitObject, Vector3 p_orgin, Vector3 explosionDir, float distance)
    {
        AIHealthScript hitHealth = hitObject.GetComponent<AIHealthScript>();

        scr_FlameSpreader temperatureScript = hitObject.GetComponent<scr_FlameSpreader>();
        Character t_character = hitObject.GetComponent<Character>();
        CharacterResourceManager resManager = hitObject.GetComponent<CharacterResourceManager>();
        if (t_character)
        {
            if (!m_spellData.isPlayerFriendly && t_character.GetIsPlayerFriendly() ||
           m_spellData.isPlayerFriendly && !t_character.GetIsPlayerFriendly())
            {
                if (hitHealth != null)
                {
                    hitHealth.TakeDamage(m_spellData.globalData.m_damage,false, explosionDir);
                }
                if (resManager != null)
                {
                    resManager.DealDamage(m_spellData.globalData.m_damage);
                }
                if (temperatureScript == null)
                {
                    temperatureScript = hitObject.gameObject.AddComponent<scr_FlameSpreader>();
                }
                if (temperatureScript != null)
                {
                    temperatureScript.ModifyCelciusWithCalculatedCelcius(m_spellData.temperatureData.spellTemperature); // need to make temperature manager to add the scripts etc. 
                }
                if (m_spellData.knockBackData.m_afflictsKnockback)
                {
                    //if (hitObject.GetComponent<NavMeshAgent>() != null)
                    //{
                    //    hitObject.GetComponent<NavMeshAgent>().Stop();
                    //    hitObject.GetComponent<Rigidbody>().isKinematic = false;
                    //}
                    if (m_spellData.knockBackData.m_knockbackDistanceFalloff == 0) //division by 0 otherwise 
                    {
                        m_spellEffects.ApplyDirectionalForce(hitObject.gameObject, explosionDir, m_spellData.knockBackData.m_knockbackForce, false);
                    }
                    else
                    {
                        m_spellEffects.ApplyDirectionalForce(hitObject.gameObject, explosionDir, m_spellData.knockBackData.m_knockbackForce / (distance * m_spellData.knockBackData.m_knockbackDistanceFalloff), false);
                    }
                }
                if (m_spellData.afflictionData.m_afflictsEffect)
                {
                    AfflictionReciever t_reciever = hitObject.GetComponent<AfflictionReciever>();
                    if (t_reciever != null)
                    {
                        t_reciever.AddCurse(new Affliction(m_spellData.afflictionData.m_afflictionData));
                    }
                    else
                    {
                        //Debug.Log("Target that trying to affect with affliction doesnt have a reciever :(");
                    }
                }
            }
        }
        else //if not character
        {
            if (hitHealth != null)
            {
                hitHealth.TakeDamage(m_spellData.globalData.m_damage,false, explosionDir);
            }
            if (resManager != null)
            {
                resManager.DealDamage(m_spellData.globalData.m_damage);
            }
            if (temperatureScript == null)
            {
                temperatureScript = hitObject.gameObject.AddComponent<scr_FlameSpreader>();
            }
            if (temperatureScript != null)
            {
                temperatureScript.ModifyCelciusWithCalculatedCelcius(m_spellData.temperatureData.spellTemperature); // need to make temperature manager to add the scripts etc. 
            }
            if (m_spellData.knockBackData.m_afflictsKnockback)
            {
                //if (hitObject.GetComponent<NavMeshAgent>() != null)
                //{
                //    hitObject.GetComponent<NavMeshAgent>().Stop();
                //    hitObject.GetComponent<Rigidbody>().isKinematic = false;
                //}
                if (m_spellData.knockBackData.m_knockbackDistanceFalloff == 0) //division by 0 otherwise 
                {
                    m_spellEffects.ApplyDirectionalForce(hitObject.gameObject, explosionDir, m_spellData.knockBackData.m_knockbackForce, false);
                }
                else
                {
                    m_spellEffects.ApplyDirectionalForce(hitObject.gameObject, explosionDir, m_spellData.knockBackData.m_knockbackForce / (distance * m_spellData.knockBackData.m_knockbackDistanceFalloff), false);
                }
            }
            if (m_spellData.afflictionData.m_afflictsEffect)
            {
                AfflictionReciever t_reciever = hitObject.GetComponent<AfflictionReciever>();
                if (t_reciever != null)
                {
                    t_reciever.AddCurse(new Affliction(m_spellData.afflictionData.m_afflictionData));
                }
                else
                {
                    //Debug.Log("Target that trying to affect with affliction doesnt have a reciever :(");
                }
            }
        }
    }
    void ApplyTerrainEffect(Vector3 pos, float p_radius)
    {
        RaycastHit hitInfo;
        LayerMask navModifiableLayer = 1 << LayerMask.NameToLayer("Nav_Modifiable");
        float spellTemp = m_spellData.temperatureData.spellTemperature;
        //freeze
        if (spellTemp < 0)
        {
            if (AffectTerrain.
                CheckForTerrainOnPoint(pos, out hitInfo, navModifiableLayer)) //checks for the terrain triggerbox
            {
                //Debug.Log(hitInfo.transform.name);
                AffectTerrain.ModifyTerrain(hitInfo.collider, pos, p_radius, DestructionShape.SHAPE_CIRCLE, 1, false);
            }
            else
            {
                Debug.Log("Found nothing");
            }
        }
        else if (spellTemp > 100)
        {
            if (AffectTerrain.CheckForTerrainOnPoint(pos, out hitInfo, navModifiableLayer)) //checks for the terrain triggerbox
            {
                //Debug.Log(hitInfo.transform.name);
                AffectTerrain.ModifyTerrain(hitInfo.collider, pos, p_radius, DestructionShape.SHAPE_CIRCLE, 0, true);
            }
            else
            {
                Debug.Log("Found nothing");
            }
        }
        //melt
    }
    void CreateExplosionEffect(Vector3 p_position, float p_radius, float p_growthTime, GameObject p_explosionEffect)
    {
        if (explosionEffect != null)
        {
            GameObject t_expl = p_explosionEffect;
            t_expl.transform.position = p_position;
            Effect_Explosion t_effect = t_expl.GetComponent<Effect_Explosion>();
            t_effect.SetGrowthTime(p_growthTime);
            t_effect.SetTargetRadius(p_radius);
        }
    }
    public void InvokeBallSpell(Vector3 spellOrgin, Vector3 spellTargetPosition, Vector3 p_spellDirection, GameObject p_projectileBase, ObjectPooler p_objectPooler)
    {
        //Vector3 targetPosition = new Vector3(m_spellData.positioningData.spellOrginX, m_spellData.positioningData.spellOrginY, m_spellData.positioningData.spellOrginZ);
        SoundPlayer.PlayRandomSoundAtPointFromArray(audioData.m_castSounds, spellTargetPosition);
        for (uint i = 0; i < m_spellData.globalData.m_spawnAmount; i++)
        {
            //temp offset
            Vector3 offset = p_spellDirection;// * 1f + Vector3.up * 1.20f;
            GameObject newBallSpell = p_projectileBase;
            newBallSpell.transform.position = spellOrgin + offset;
            SpellProjectile t_spellProj = newBallSpell.AddComponent<SpellProjectile>();
            t_spellProj.CustomConstructor(m_spellData, p_spellDirection, explosionEffect, audioData, p_objectPooler);

        }
    }
    public void InvokeAirToTargetSpell(Vector3 spellOrgin, Vector3 spellTargetPosition, Vector3 p_spellDirection, float p_spawnHeight, GameObject p_projectileBase, ObjectPooler p_objectPooler)
    {
        //set position in random point above
        //get dir to targetPos

        //Vector3 targetPosition = new Vector3(m_spellData.positioningData.spellOrginX, m_spellData.positioningData.spellOrginY, m_spellData.positioningData.spellOrginZ);
        SoundPlayer.PlayRandomSoundAtPointFromArray(audioData.m_castSounds, spellTargetPosition);
        for (uint i = 0; i < m_spellData.globalData.m_spawnAmount; i++)
        {
            //temp offset
            //Vector3 offset = p_spellDirection * 1f + Vector3.up * 1.20f;
            GameObject newBallSpell = p_projectileBase;
            Vector3 spawnVertHorOffset = Vector3.right * m_spellData.aoeData.m_radius + Vector3.forward * m_spellData.aoeData.m_radius;
            Vector3 spawnRandomOffset = new Vector3(Random.Range(-spawnVertHorOffset.x, spawnVertHorOffset.x), Random.Range(-spawnVertHorOffset.y, spawnVertHorOffset.y), Random.Range(-spawnVertHorOffset.z, spawnVertHorOffset.z));
            Vector3 airSpawnPos = (spellTargetPosition + (Vector3.up * p_spawnHeight) + spawnRandomOffset);
            Vector3 newSpellDir = Vector3.Normalize((spellTargetPosition - airSpawnPos));
            newBallSpell.transform.position = airSpawnPos;
            SpellProjectile t_spellProj = newBallSpell.AddComponent<SpellProjectile>();
            t_spellProj.CustomConstructor(m_spellData, newSpellDir, explosionEffect, audioData, p_objectPooler);
        }
    }
    public SpellData GetSpellData()
    {
        return m_spellData;
    }
}
