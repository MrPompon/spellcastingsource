﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public struct AudioData
{
    public AudioClip[] m_castSounds;
    public AudioClip[] m_spellTriggerSounds;
}
public class SpellBase{

    //all
    public SpellData m_spellData;
    public GameObject explosionEffect;
    public AudioData audioData;
      [System.Serializable]
    public struct SpellData
    {
          

          [HideInInspector]
        public SpellEffects m_spellEffects;
        public SpellTypeEnums.SpellTypeAffected m_typeAffected;
        public SpellTypeEnums.SpellTypeProjectile m_typeProjectile;
        public SpellTypeEnums.SpellProjectileCollisionBehaviour m_spellCollisionBehavior;
        public SpellTypeEnums.SpellCastingType m_spellCastingType;
        [HideInInspector]
        //public GameObject m_originator;
        public string m_name;
        public string m_description;
        public LayerData layerData;
        public GlobalData globalData;
        public ProjectileData projectileData;
        public ChannelingData channelingData;
        public BounceData bounceData;
        public KnockBackData knockBackData;
        public HomingData homingData;
        public PulseData pulseData;
        public DotData dotData;
        public AOEData aoeData; 
        public AfflictionLocalData afflictionData;
        public PositioningData positioningData;
        public TemperatureData temperatureData;
        public bool isBloodBased; //if bloodbased or soul the cast cost in mana is reduced and the other modifiersmaybe moddified, BUT it will require blood or bones or something around a certain point with the original aoe of the spell and drain from them, the bloodcost will be dependant on original manacost(like ulti building)
        //public Sprite spellUIImage;
        public float calculatedManaCost;
        public bool isPlayerFriendly;
       
          [System.Serializable]
        public struct LayerData
        {
            public int m_obstacleLayer;
            public int m_affectedLayer;
        }
            [System.Serializable]
        public struct GlobalData
        {
            public int m_spawnAmount;
            public float m_range;
            public float m_damage;
            public bool m_spellInitiated;
        }
            [System.Serializable]
        public struct ProjectileData
        {
            public float m_projectileSpeed;
            public float m_duration;
        }
          [System.Serializable]
        public struct AOEData
        {
            public float m_radius;
        }
            [System.Serializable]
        public struct ChannelingData
        {
            public float m_channelingProgress;
            public float m_channelDuration;
            public bool m_isChannelCast;
            public bool m_canMoveWhileChanneling;
            public float m_channelCastRate;
            public bool m_isCurrentlyBeingChanneled;
            public float m_channelInstances;
            public int m_currentChannelInstance;
            public float m_currentChannelInstanceProgress; 
        }
            [System.Serializable]
        public struct BounceData
        {
            public int m_bounceAmount;
            public bool m_triggerEffectOnBounce;
        }
            [System.Serializable]
        public struct KnockBackData
        {
            public bool m_afflictsKnockback;
            public float m_knockbackForce;
            public float m_knockbackDistanceFalloff;
        }
            [System.Serializable]
        public struct HomingData
        {
            public bool m_isHoming;
            public float m_homingTurnRate; 
        }
            [System.Serializable]
        public struct PulseData
        {
            public bool m_IsTriggeredWithInterval;
            public float m_triggerIntervalRate;
        }
            [System.Serializable]
        public struct DotData
        {
            public int m_dotAmount;
            public float m_dotRate;
        }
            [System.Serializable]
        public struct AfflictionLocalData
        {
            public bool m_afflictsEffect;
            public float m_afflictionDuration;
            public SpellEffects.AfflictionData m_afflictionData;
        }
          [System.Serializable]
            public struct TemperatureData
            {
              public float spellTemperature;
            }
          [System.Serializable]
            public struct PositioningData
            {
                public float spellOrginX;
                public float spellOrginY;
                public float spellOrginZ;
            }
    }
}
