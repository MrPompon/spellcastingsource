﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterEquipment : MonoBehaviour {

	// Use this for initialization
    public Equipped m_equipped = new Equipped(0);
    [HideInInspector]
    public SpellEffects.AfflictionData m_equipmentData;
     [HideInInspector]
    public SpellEffects.AfflictionData m_resetToData;
    [System.Serializable]
    public struct Equipped // make an equipManager and insert helm etc here, shouldnt define the structs here. 
    {
        //[HideInInspector]
        public List<Equipment> list_equipped;
        public Equipment equipped_helm;
        public Equipment equipped_chest;
        public Equipment equipped_waist;
        public Equipment equipped_glove_l;
        public Equipment equipped_glove_r;
        public Equipment equipped_ring_l;
        public Equipment equipped_ring_r;
        public Equipment equipped_neck;
        public Equipment equipped_leg_l;
        public Equipment equipped_leg_r;
        public Equipment equipped_boot_l;
        public Equipment equipped_boot_r;
        public Equipment equipped_shoulder_l;
        public Equipment equipped_shoulder_r;
        public Equipment equipped_lh;
        public Equipment equipped_rh;
        public Equipped(int swag)
        {
            list_equipped = new List<Equipment>(); //new equipment might be problem, list equipped != the equipment slots, desync code checks equipped list, and it is not updated correctly.

            equipped_helm = new Equipment();
            list_equipped.Add(equipped_helm);


            equipped_chest = new Equipment();
            list_equipped.Add(equipped_chest);

            equipped_waist = new Equipment();
            list_equipped.Add(equipped_waist);

            equipped_glove_l = new Equipment();
            list_equipped.Add(equipped_glove_l);

            equipped_glove_r = new Equipment();
            list_equipped.Add(equipped_glove_r);

            equipped_ring_l = new Equipment();
            list_equipped.Add(equipped_ring_l);

            equipped_ring_r = new Equipment();
            list_equipped.Add(equipped_ring_r);

            equipped_neck = new Equipment();
            list_equipped.Add(equipped_neck);

            equipped_leg_l = new Equipment();
            list_equipped.Add(equipped_leg_l);

            equipped_leg_r = new Equipment();
            list_equipped.Add(equipped_leg_r);

            equipped_boot_l = new Equipment();
            list_equipped.Add(equipped_boot_l);

            equipped_boot_r = new Equipment();
            list_equipped.Add(equipped_boot_r);

            equipped_shoulder_l = new Equipment();
            list_equipped.Add(equipped_shoulder_l);

            equipped_shoulder_r = new Equipment();
            list_equipped.Add(equipped_shoulder_r);

            equipped_lh = new Equipment();
            list_equipped.Add(equipped_lh);

            equipped_rh = new Equipment();
            list_equipped.Add(equipped_rh);
        }
    
    }
    public void DeequipEquipment(ref Equipment targetEquipment)
    {
        //temp 
        targetEquipment = null;
    }
       
      
    public void EquipEquipment(ref Equipment p_swapEquip,ref Equipment p_equipment)
    {
        p_swapEquip.m_equipdata = p_equipment.m_equipdata;
        p_swapEquip.m_baseDef = p_equipment.m_baseDef;
        p_swapEquip.m_baseDefRate = p_equipment.m_baseDefRate;
        //p_swapEquip.m_equipModel = p_equipment.m_equipModel;
        p_swapEquip.m_equipType = p_equipment.m_equipType;
        p_swapEquip.m_magicRes = p_equipment.m_magicRes;
        p_swapEquip.m_weight = p_equipment.m_weight;
        p_swapEquip.m_equipType = p_equipment.m_equipType;
        RecalculateEquipmentStats();
    }
    public void QuickSwapEquipment(ref Equipment p_newEquipment)
    {
        switch (p_newEquipment.m_equipType)
        {
            case Equipment.EquipmentType.TYPE_BOOTS:
                break;
            case Equipment.EquipmentType.TYPE_CHEST:
                break;
            case Equipment.EquipmentType.TYPE_HANDS:
                break;
            case Equipment.EquipmentType.TYPE_HELMET:
                break;
            case Equipment.EquipmentType.TYPE_LEGS:
                break;
            case Equipment.EquipmentType.TYPE_NECK:
                break;
            case Equipment.EquipmentType.TYPE_RING:
                break;
            case Equipment.EquipmentType.TYPE_WAIST:
                break;
            case Equipment.EquipmentType.TYPE_WEAPON:
                break;
        }
    }
     public void RecalculateEquipmentStats()
     {
         ResetEquipmentData();
         foreach (Equipment equ in m_equipped.list_equipped)
         {
             m_equipmentData.armorMods.sm_armorMod += equ.m_equipdata.armorMods.sm_armorMod;
             m_equipmentData.armorMods.sm_armorMultiplier += equ.m_equipdata.armorMods.sm_armorMultiplier;

             m_equipmentData.damageAmpMods.sm_damageDealtMultiplier += equ.m_equipdata.damageAmpMods.sm_damageDealtMultiplier;

             m_equipmentData.healthMods.sm_hpMaxMod += equ.m_equipdata.healthMods.sm_hpMaxMod;
             m_equipmentData.healthMods.sm_hpRegMod += equ.m_equipdata.healthMods.sm_hpRegMod;

             m_equipmentData.manaMods.sm_manaMaxMod += equ.m_equipdata.manaMods.sm_manaMaxMod;
             m_equipmentData.manaMods.sm_manaRegMod += equ.m_equipdata.manaMods.sm_manaRegMod;
             //Debug.Log(m_equipmentData.manaMods.sm_manaRegMod);
             m_equipmentData.offensiveMods.sm_attackSpeedMod += equ.m_equipdata.offensiveMods.sm_attackSpeedMod;
             m_equipmentData.offensiveMods.sm_castSpeedMod += equ.m_equipdata.offensiveMods.sm_castSpeedMod;

             m_equipmentData.recievedDamageAmpMods.sm_damageRecievedMultiplier += equ.m_equipdata.recievedDamageAmpMods.sm_damageRecievedMultiplier;

             m_equipmentData.velocityMods.sm_speedMod += equ.m_equipdata.velocityMods.sm_speedMod;

             m_equipmentData.resistanceMods.sm_allResistanceMod += equ.m_equipdata.resistanceMods.sm_allResistanceMod;
             m_equipmentData.resistanceMods.sm_darknessResistanceMod += equ.m_equipdata.resistanceMods.sm_darknessResistanceMod;
             m_equipmentData.resistanceMods.sm_fireResistanceMod += equ.m_equipdata.resistanceMods.sm_fireResistanceMod;
             m_equipmentData.resistanceMods.sm_holyResistanceMod += equ.m_equipdata.resistanceMods.sm_holyResistanceMod;
             m_equipmentData.resistanceMods.sm_iceResistanceMod += equ.m_equipdata.resistanceMods.sm_iceResistanceMod;
             m_equipmentData.resistanceMods.sm_lightningResistanceMod += equ.m_equipdata.resistanceMods.sm_lightningResistanceMod;
             m_equipmentData.resistanceMods.sm_venomResistanceMod += equ.m_equipdata.resistanceMods.sm_venomResistanceMod;
             m_equipmentData.resistanceMods.sm_waterResistanceMod += equ.m_equipdata.resistanceMods.sm_waterResistanceMod;

            //m_equipmentData.healthMods.sm_hpMaxMod = 9999999f;//TEST
            //m_equipmentData.resistanceMods.sm_waterResistanceMod = 9999999f;//TEST
            //Debug.Log(equ.m_equipdata.healthMods.sm_hpMaxMod + "On Item" + equ.m_equipdata.name);
            //Debug.Log(m_equipped.list_equipped.Count);
        }
     }
     void ResetEquipmentData()
     {
         m_equipmentData = m_resetToData;
        ResetEquippedList();
     }
    void ResetEquippedList() // new
    {
        m_equipped.list_equipped.Clear();
        m_equipped.list_equipped.Add(m_equipped.equipped_helm);
        m_equipped.list_equipped.Add(m_equipped.equipped_chest);
        m_equipped.list_equipped.Add(m_equipped.equipped_waist);
        m_equipped.list_equipped.Add(m_equipped.equipped_glove_l);
        m_equipped.list_equipped.Add(m_equipped.equipped_glove_r);
        m_equipped.list_equipped.Add(m_equipped.equipped_ring_l);
        m_equipped.list_equipped.Add(m_equipped.equipped_ring_r);
        m_equipped.list_equipped.Add(m_equipped.equipped_neck);
        m_equipped.list_equipped.Add(m_equipped.equipped_leg_l);
        m_equipped.list_equipped.Add(m_equipped.equipped_leg_r);
        m_equipped.list_equipped.Add(m_equipped.equipped_boot_l);
        m_equipped.list_equipped.Add(m_equipped.equipped_boot_r);
        m_equipped.list_equipped.Add(m_equipped.equipped_shoulder_l);
        m_equipped.list_equipped.Add(m_equipped.equipped_shoulder_r);
        m_equipped.list_equipped.Add(m_equipped.equipped_lh);
        m_equipped.list_equipped.Add(m_equipped.equipped_rh);
}
}
