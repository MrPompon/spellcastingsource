﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CharacterSpellBook : MonoBehaviour {

    private List<Spell> m_spellList;
    SpellManaCostCalculator m_calculator;
    void Start()
    {
        m_calculator = GameObject.FindGameObjectWithTag("SpellLibrary_Tag").GetComponent<SpellManaCostCalculator>();
    }
    public List<Spell> GetSpellList()
    {
        return m_spellList;
    }
    public void AddSpell(Spell p_spell)
    {
        m_spellList.Add(p_spell);
    }
    public void CheckSpellCost(Spell p_spell)
    {
        //reduce owners mana pool by this, check if enough mana first.
        m_calculator.CalculateManaCost(p_spell.GetSpellData());
    }
}
