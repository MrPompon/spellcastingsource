﻿using UnityEngine;
using System.Collections;

public class CharacterBase : MonoBehaviour {
    public CharacterStats m_Stats;
    [System.Serializable]
    public struct CharacterStats
    {
        public StatAttributes statAttributes;
        public MovementAttributes movementAttributes;
        public OffensiveAttributes offensiveAttributes;
        public DefensiveAttributes defensiveAttributes;
        public Resistances resistanceAttributes;
        public HealthAttributes healthAttributes;
        public ManaAttributes manaAttributes;
        public DamageAmpAttributes damageAmpAttributes;
        public RecievedDamageAmpAttributes recDamageAmpAttributes;
         [System.Serializable]
        public struct StatAttributes
        {
            public int strength;
            public int agility;
            public int intelligence;
            public int wisdom;
            public int dexterity;
        }
         [System.Serializable]
        public struct MovementAttributes
        {
            public float turnRate;
            public float moveSpeed;
        }
         [System.Serializable]
        public struct OffensiveAttributes
        {
            public float castSpeed;
            public float attackSpeed;
        }
         [System.Serializable]
        public struct DefensiveAttributes
        {
            public int armor;
        }
         [System.Serializable]
        public struct HealthAttributes
        {
            public int health;
            public float healthReg;
        }
         [System.Serializable]
        public struct ManaAttributes
        {
            public int mana;
            public float manaReg;
        }
         [System.Serializable]
        public struct Resistances
        {
            public float allResistance;
            public float lightningResistance;
            public float fireResistance;
            public float iceResistance;
            public float waterResistance;
            public float venomResistance;
            public float darknessResistance;
            public float holyResistance;
        }
         [System.Serializable]
        public struct DamageAmpAttributes
        {
            public float damageAmp;
        }
         [System.Serializable]
        public struct RecievedDamageAmpAttributes
        {
            public float recDamageAmp;
        }
    }
}
