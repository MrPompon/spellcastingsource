﻿using UnityEngine;
using System.Collections;

public class SpellBehaviour : SpellType {
    public GameObject m_projectileBase;

 
    public void ChannelSpell()
    {
       m_spellData.channelingData.m_channelingProgress += Time.deltaTime;
    }
    public bool IsSpellChannelDone()
    {
        if (m_spellData.channelingData.m_channelingProgress >= m_spellData.channelingData.m_channelDuration)
        {
            return true;
        }
        return false;
    }
    public bool IsSelfChannelDone() //autochannel/delayed spell
    {
        m_spellData.channelingData.m_channelingProgress += Time.deltaTime;
        if (m_spellData.channelingData.m_channelingProgress >= m_spellData.channelingData.m_channelDuration)
        {
            return true;
        }
        return false;
    }
    public void InterruptChannel()
    {
        ResetChannelling();
    }
    public void ResetChannelling()
    {
        m_spellData.channelingData.m_channelingProgress = 0;
    }
	// Update is called once per frame

}
