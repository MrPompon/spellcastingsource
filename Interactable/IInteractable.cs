﻿using UnityEngine;
using System.Collections;

public interface IInteractable
{
    void Hover();
    void Interact();
    void SetInteractable(bool ToF);
}
