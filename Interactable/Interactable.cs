﻿using UnityEngine;
using System.Collections;

public class Interactable : MonoBehaviour, IInteractable {

	// Use this for initialization
    public GameObject gameManager;
    public GameManagerFunctions gMF;
    public float interactableRange;
    public bool isInteractableFromStart;
    private Transform highlightCircle;
    private MeshRenderer hLCMR;
    private Material outlineMat;
    private bool isInteractable;
    private bool isHighlighted = false;
	public virtual void Start () {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        gMF = gameManager.GetComponent<GameManagerFunctions>();
        isInteractable = isInteractableFromStart;
        highlightCircle = transform.Find("HighlightCircle");
        hLCMR = highlightCircle.GetComponent<MeshRenderer>();
        outlineMat=Resources.Load("G/Materials/ItemOutlineMat") as Material;
	}
    public virtual void OnMouseEnter()
    {
        CheckHighlightConditions();
        //hLCMR.material = outlineMat;
    }
    public virtual void OnMouseExit()
    {
        hLCMR.enabled = false;
        isHighlighted = false;
        //hLCMR.material = null;
    }
    public virtual void OnMouseOver()
    {
        if (isHighlighted)
        {
            Hover();
        }
        else if (!isHighlighted)
        {
            CheckHighlightConditions();
        }
    }
    void CheckHighlightConditions()
    {
        if (isInteractable)
        {
            if (gMF.IsPlayerInSight(this.transform, interactableRange))
            {
                if (gMF.IsPlayerDistanceCloserThen(this.transform, interactableRange))
                {
                    hLCMR.enabled = true;
                    isHighlighted = true;
                }
            }
        }
    }
    public void Hover()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Interact();
        }
    }
    public virtual void Interact()
    {
        Debug.Log("Trying to interact");
    }
    public void SetInteractable(bool ToF)
    {
        isInteractable = ToF;
    }
}
