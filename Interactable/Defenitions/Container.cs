﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Container : Interactable {

    public ContainerGrade m_grade;
    public int containerSize;
    private List<Equipment> m_containmentList;
    GameObject m_gameManager;
    EquipmentGeneration m_EquipGenerator;
    public Transform chestLock;
    Animator chestLockAnimator;
    public AudioClip openingSound;
	// Use this for initialization
    public enum ContainerGrade
    {
        GRADE_BASIC,
        GRADE_COMMON,
        GRADE_UNCOMMON,
        GRADE_RARE,
        GRADE_EPIC,
        GRADE_LEGENDARY,
    }
    public bool isLocked;
    public float lockLevel;

	public override void Start () 
    {
        if (chestLock == null)
        {
            chestLock = this.transform.Find("Chest_Top");
        }
        chestLockAnimator = chestLock.GetComponent<Animator>();
        m_gameManager= GameObject.FindGameObjectWithTag("GameManager");
        m_EquipGenerator = m_gameManager.GetComponent<EquipmentGeneration>();
        m_containmentList = new List<Equipment>();
        if (m_EquipGenerator.pickupableEquipPrefab == null)
        {
            Debug.Log("Pickupable Item Prefab in ItemGenerator is null :("); //do resources load when i have path
        }
        base.Start();
        GenerateChestContainment(m_grade);
	}

    public override void Interact()
    {
        AttemptToOpen();
    }
    public void GenerateChestContainment(ContainerGrade m_containerGrade)
    {
        float rarityMin=0, rarityMax=0, modScaleMin=0, modScaleMax =0;
        int modScaleGradeModifier  = 12;
        int modScaleRarityModifier = 10;
        switch (m_containerGrade)
        {
            case ContainerGrade.GRADE_BASIC:
                rarityMin = 0;
                rarityMax = modScaleRarityModifier;
                modScaleMin = 0;
                modScaleMax = modScaleGradeModifier;
                break;
            case ContainerGrade.GRADE_COMMON:
                rarityMin = modScaleRarityModifier;
                rarityMax = modScaleRarityModifier * 2;
                modScaleMin = modScaleGradeModifier;
                modScaleMax = modScaleGradeModifier * 2;
                break;
            case ContainerGrade.GRADE_UNCOMMON:
                rarityMin = modScaleRarityModifier * 2;
                rarityMax = modScaleRarityModifier * 3;
                modScaleMin = modScaleGradeModifier * 2;
                modScaleMax = modScaleGradeModifier * 3;
                break;
            case ContainerGrade.GRADE_RARE:
                rarityMin = modScaleRarityModifier * 3;
                rarityMax = modScaleRarityModifier * 4;
                modScaleMin = modScaleGradeModifier * 3;
                modScaleMax = modScaleGradeModifier * 4;
                break;
            case ContainerGrade.GRADE_EPIC:
                rarityMin = modScaleRarityModifier * 4;
                rarityMax = modScaleRarityModifier * 5;
                modScaleMin = modScaleGradeModifier * 4;
                modScaleMax = modScaleGradeModifier * 5;
                break;
            case ContainerGrade.GRADE_LEGENDARY:
                rarityMin = modScaleRarityModifier * 5;
                rarityMax = modScaleRarityModifier * 6;
                modScaleMin = modScaleGradeModifier * 5;
                modScaleMax = modScaleGradeModifier * 6;
                break;
        }
        for (uint i = 0; i < containerSize; i++)
        {
            GenerateObjectToContainment(rarityMin, rarityMax, modScaleMin, modScaleMax);
        }
    }
    void GenerateObjectToContainment(float p_rarityMin, float p_rarityMax, float p_modScaleMin, float p_modScaleMax)
    {
        int amountOfItemModsOnItem = 3;
        float itemRarity = Random.Range(p_rarityMin, p_rarityMax);
        float modScale = Random.Range(p_modScaleMin, p_modScaleMax);
        //add a new objects from some kind of pool to list
        Equipment generatedEquip = new Equipment();
        string itemName="";
        m_EquipGenerator.GenerateName(ref itemName);
        generatedEquip.m_equipdata.name = itemName;
        int rndItemType = Random.Range(0, (int)EquipmentBase.EquipmentType.TYPE_AMOUNT);
       
        for (int i = 0; i < amountOfItemModsOnItem; i++)
        {
            m_EquipGenerator.GenerateItemMods(ref generatedEquip.m_equipdata, itemRarity, modScale);
        }
        m_EquipGenerator.DecideItemType(ref generatedEquip.m_equipType, rndItemType);
        m_containmentList.Add(generatedEquip);
        // Debug.Log(itemName);
        //Debug.Log(itemRarity.ToString() + "Rarity" + modScale.ToString() + "ModScale");
    }

    void AttemptToOpen()
    {
        //check skill level here
        Open();
    }
    void AnimateChestLock(Animator p_chestAnimation)
    {
        //p_chestAnimation.SetBool("anim_IsOpened",true);
        //p_chestAnimation.Play(,);
    }
    void PlayOpeningSound(AudioClip p_openSound)
    {
        if (p_openSound)
        {
            AudioSource.PlayClipAtPoint(p_openSound, this.transform.position);
        }
    }
    void Open()
    {
        PlayOpeningSound(openingSound);
        AnimateChestLock(chestLockAnimator);
        SpawnContainment();
        //Destroy(this.gameObject); //animate and set not interactable
    }
    void SpawnContainment() //note to self NEED TO ADD THESE VALUES FROM GENERATION TO HERE FOR BASESTATS BECAUSE FU 
    {
        for (int i = 0; i < m_containmentList.Count; i++)
        {
            GameObject itemInstance= Instantiate(m_EquipGenerator.pickupableEquipPrefab, this.transform.position, Quaternion.identity) as GameObject;
            Pickupable_Equipment temp = itemInstance.GetComponent<Pickupable_Equipment>();
            temp.m_equipData = m_containmentList[i].m_equipdata;
            temp.m_equip.m_equipType = m_containmentList[i].m_equipType;
            itemInstance.name = m_containmentList[i].m_equipdata.name;
            itemInstance.transform.parent = m_EquipGenerator.treasureParent;
        }
        m_containmentList.Clear();   
        //spawn objects from the object list
    }
    void TranferGeneratedDataToPickupable()
    {

    }
}
