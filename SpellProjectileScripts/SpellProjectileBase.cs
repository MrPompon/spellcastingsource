﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class SpellProjectileBase : MonoBehaviour {
    public SpellBase.SpellData m_spellData;
    //public SpellEffects m_spellEffects;
    //public SpellEffects.AfflictionData m_afflictionData;
    //public SpellTypeEnums.SpellTypeAffected m_typeAffected;
    //public SpellTypeEnums.SpellTypeProjectile m_typeProjectile;
    //public SpellTypeEnums.SpellProjectileCollisionBehaviour m_spellCollisionBehavior;
    //public LayerMask m_obstacleLayer;
    //public LayerMask m_affectedLayer;
    //public string m_name;

    //public float m_manaCost; //might affect some specific spell type, otherwise unessecary
    //public float m_duration;
    //public float m_range;    //the manimum distance from original point to currentpoint
    //public float m_damage;

    //public GameObject m_originator;
    ////aoe
    //public float m_radius; //explosion radius
    ////dot
    //public int m_dotAmount; //if applies curse or dot
    //public float m_dotRate;

    public Rigidbody m_rigid;
    public Collider m_colli;
    public bool m_hasStartedPulsing;
    public float firstPulseCooldown;
    ////bounce
    //public int m_bounceAmount;
    //public bool m_triggerEffectOnBounce;
    ////proctile variables
    //public float m_HomingTurnRate;
    //public bool m_isHoming;
    //public Vector3 m_velocity;
    //public float m_projectileSpeed;
    ////Knockback /force
    //public bool m_afflictsKnockback;
    //public float m_knockbackForce;
    //public float m_knockbackDistanceFalloff;
    ////pulse
    //public bool m_IsTriggeredWithInterval;
    //public float m_triggerIntervalRate;
    ////affliction
    //public bool m_afflictsEffect;
    //public float m_afflictionDuration;
    public Collider m_closestTarget;
}
