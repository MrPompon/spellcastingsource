﻿using UnityEngine;
using System.Collections;

public class SpellProjectile : SpellProjectileBehaviour
{

    // Use this for initialization
    //public void CustomConstructor(GameObject originator,
    //    string p_name,
    //    SpellTypeEnums.SpellTypeProjectile p_typeProjectile,
    //    SpellTypeEnums.SpellTypeAffected p_typeAffected,
    //    SpellTypeEnums.SpellProjectileCollisionBehaviour p_collisionBehaviour,
    //    float p_damage,float p_radius, float p_projectileSpeed,bool p_isHoming,float p_HomingTurnRate, int p_bounceAmount, bool p_triggerEffectOnBounce,
    //    bool p_knocksBack, float p_knockbackForce, float p_knockBackDistanceFalloff,
    //    bool p_isTriggeredWithInterval, float p_triggerInterval, bool p_afflictsEffect,float p_afflictionDuration,
    //    LayerMask p_affectedLayer, LayerMask p_obstacleLayer, SpellEffects.AfflictionData p_afflctionData)
    //{
    //    m_spellEffects = GameObject.FindGameObjectWithTag("SpellLibrary_Tag").GetComponent<SpellEffects>() ;
    //    m_rigid = GetComponent<Rigidbody>();
    //    m_name = p_name;
    //    m_originator = originator;
    //    m_typeProjectile = p_typeProjectile;
    //    m_isHoming = p_isHoming;
    //    m_typeAffected = p_typeAffected;
    //    m_spellCollisionBehavior = p_collisionBehaviour;
    //    m_damage = p_damage;
    //    m_radius = p_radius;
    //    m_projectileSpeed = p_projectileSpeed;
    //    m_bounceAmount = p_bounceAmount;
    //    m_triggerEffectOnBounce = p_triggerEffectOnBounce;
    //    SetupRigidBody();
    //    SetupCollider();
    //    SetupCollision();
    //    //Physics.IgnoreCollision(originator.GetComponent<Collider>(), m_colli); //<- might disable self hitting? 
    //    LaunchProjectile(m_projectileSpeed,this.transform.forward);
    //    m_affectedLayer = p_affectedLayer;
    //    m_obstacleLayer = p_obstacleLayer;

    //    m_afflictsKnockback = p_knocksBack;
    //    m_knockbackForce = p_knockbackForce;
    //    m_knockbackDistanceFalloff = p_knockBackDistanceFalloff;

    //    m_IsTriggeredWithInterval = p_isTriggeredWithInterval;
    //    m_triggerIntervalRate = p_triggerInterval;
    //    m_afflictsEffect = p_afflictsEffect;
    //    m_afflictionDuration = p_afflictionDuration;
    //    m_HomingTurnRate = p_HomingTurnRate;
    //    m_afflictionData = p_afflctionData;
    //    if (m_isHoming)
    //    {
    //        m_closestTarget = FindandGetClosestTarget();
    //    }
    //}
    public void CustomConstructor(SpellBase.SpellData p_spellData, Vector3 spellDirection, GameObject p_explosionEffect, AudioData p_audioData, ObjectPooler p_objectPooler)
    {
        SetObjectPooler(p_objectPooler);
        audioData = p_audioData;
        explosionEffect = p_explosionEffect;
        m_spellData = p_spellData;
        SetupRigidBody();
        SetupCollider();
        SetupCollision();
        //Physics.IgnoreCollision(originator.GetComponent<Collider>(), m_colli); //<- might disable self hitting? 
        firstPulseCooldown = m_spellData.pulseData.m_triggerIntervalRate;
        LaunchProjectile(m_spellData.projectileData.m_projectileSpeed, spellDirection);

        if (m_spellData.homingData.m_isHoming)
        {
            m_closestTarget = FindandGetClosestTarget();
        }
    }
    //{}
    void SetupProjectileBasedOnType()
    {

    }
    void SetupCollision()
    {
        switch (m_spellData.m_spellCollisionBehavior)
        {
            case SpellTypeEnums.SpellProjectileCollisionBehaviour.COLLISION_BOUNCE:
                PhysicMaterial temp = new PhysicMaterial(); //temporary
                temp.bounciness = 1.0f;
                temp.staticFriction = 0.3f;
                temp.name = "Custom Bounce Material";
                temp.bounceCombine = PhysicMaterialCombine.Maximum;
                m_colli.material = temp;
                break;
            case SpellTypeEnums.SpellProjectileCollisionBehaviour.COLLISION_EXPLODE:
                m_colli.material = null;
                break;
            case SpellTypeEnums.SpellProjectileCollisionBehaviour.COLLISION_PIERCE:
                //probably need to convert the collision to trigger here
                //then maybe reduce its velocity depening on what it hits/pierces, pierce power?
                m_colli.material = null;
                break;
        }

    }
    void SetupRigidBody()
    {
        m_rigid = GetComponent<Rigidbody>();
        m_rigid.drag = 0;
        m_rigid.angularDrag = 0;
        m_rigid.mass = 100;
    }
    void SetupCollider()
    {
        m_colli = GetComponent<Collider>();
    }
    void LaunchProjectile(float p_speed, Vector3 p_dir)
    {
        m_rigid.AddForce(p_speed * p_dir, ForceMode.VelocityChange);
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (m_spellData.homingData.m_isHoming)
        {
            if (m_closestTarget == null)
            {
                m_closestTarget = FindandGetClosestTarget();  //<---- unoptimized.
                if (m_closestTarget == null)
                {
                    return;
                }
            }
            HomeTowardsPoint(m_closestTarget.transform.position);
        }
    }
    void Update()
    {
        base.Update();
    }

    //void OnCollisionEnter(Collision colli)
    //{
    //    Debug.Log("CollisionEnter");
    //    base.OnCollisionEnter(colli);
    //}
    void OnTriggerEnter(Collider colli)
    {
        base.OnTriggerEnter(colli);
    }
    
}
