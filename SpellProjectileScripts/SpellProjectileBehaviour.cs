﻿using UnityEngine;
using System.Collections;

public class SpellProjectileBehaviour : SpellProjectileBase {
    public GameObject m_projectileBase;
    public GameObject explosionEffect;
    public ObjectPooler objectPooler;
    public AudioData audioData;
    float lifeTime = 0;
	// Use this for initialization
	void Start () {
	}
	// Update is called once per frame
	public virtual void Update () {
        lifeTime += Time.deltaTime;
        if (lifeTime > m_spellData.projectileData.m_duration)
        {
            Explode(this.transform.position);
            Despawn();
        }
        if (!m_hasStartedPulsing)
        {
            if (m_spellData.pulseData.m_IsTriggeredWithInterval)
            {
                if (firstPulseCooldown < 0)
                {
                    StartCoroutine(Pulse(m_spellData.pulseData.m_triggerIntervalRate));
                    m_hasStartedPulsing = true;
                }
                else
                {
                    firstPulseCooldown -= Time.deltaTime;
                }
            }
        }
	}
    IEnumerator Pulse(float pulseRate)
    {
        while (true)
        {
            TriggerSpellEffect(false);
            yield return new WaitForSeconds(pulseRate);
        }
    }
    public virtual void OnTriggerEnter(Collider colli)
    {
        if (!colli.isTrigger) //probably main collision handler
        {
            //switch (m_spellCollisionBehavior)
            //{
            //    case SpellTypeEnums.SpellProjectileCollisionBehaviour.COLLISION_BOUNCE:
            //        Bounce(colli);
            //        break;
            //    case SpellTypeEnums.SpellProjectileCollisionBehaviour.COLLISION_EXPLODE:
            //        Explode();
            //        break;
            //    case SpellTypeEnums.SpellProjectileCollisionBehaviour.COLLISION_PIERCE:
            //        Pierce();
            //        break;
            //}
        }
    }
    public virtual void OnCollisionEnter(Collision colli)
    {
        if (!colli.collider.isTrigger)
        {
            switch (m_spellData.m_spellCollisionBehavior)
            {
                case SpellTypeEnums.SpellProjectileCollisionBehaviour.COLLISION_BOUNCE:
                    Bounce(colli);
                    break;
                case SpellTypeEnums.SpellProjectileCollisionBehaviour.COLLISION_EXPLODE:
                    TriggerSpellEffect(true);
                    break;
                case SpellTypeEnums.SpellProjectileCollisionBehaviour.COLLISION_PIERCE:
                    Pierce();
                    break;
            }
        }
    }
    void Pierce()
    {

    }
    void CreateExplosionEffect(Vector3 p_position, float p_radius, float p_growthTime, GameObject p_explosionEffect)
    {
        if (explosionEffect != null)
        {
            GameObject t_expl = p_explosionEffect;
            t_expl.transform.position = p_position;
            Effect_Explosion t_effect = t_expl.GetComponent<Effect_Explosion>();
            t_effect.SetGrowthTime(p_growthTime);
            t_effect.SetTargetRadius(p_radius);
        }
    
    }
    void Bounce(Collision colli)
    {
        m_spellData.bounceData.m_bounceAmount--;
        if (m_spellData.bounceData.m_bounceAmount < 0)
        {
            Explode(this.transform.position);
            Despawn();
        }
        else if (m_spellData.bounceData.m_triggerEffectOnBounce)
        {
            TriggerSpellEffect(false);
        }
    }
    void TriggerSpellEffect(bool p_despawnAfter)
    {
        Explode(this.transform.position);
        if (p_despawnAfter)
        {
            Despawn();
        }
    }
    void Explode(Vector3 p_orgin)
    {
        //Debug.Log(LayerMask.LayerToName(m_spellData.layerData.m_affectedLayer));
        objectPooler.CreateExplosionEffect(p_orgin,0.25f,m_spellData.aoeData.m_radius);
        SoundPlayer.PlayRandomSoundAtPointFromArray(audioData.m_spellTriggerSounds, p_orgin);
        //Debug.Log("Spell Exploded");
        Collider[] hits = GlobalRaycast.SphereCastForObjectsWithinRange(p_orgin, m_spellData.aoeData.m_radius, m_spellData.layerData.m_affectedLayer);
        //Debug.Log(hits.Length + " before wall refine");
        //for (uint i = 0; i < hits.Length; i++)
        //{

        //    Debug.Log(hits[i].transform.name);
        //}

        hits = GlobalRaycast.RefineCollidersInExplosionFOV(hits, p_orgin, m_spellData.layerData.m_obstacleLayer);
        for (uint i = 0; i < hits.Length; i++)
        {
            //Debug.Log(hits[i].transform.name);
            Vector3 dir= GlobalRaycast.GetDirectionBetween(p_orgin, hits[i].transform.position);
            float distance = Vector3.Distance(hits[i].transform.position, p_orgin);
            ExplodeConditionals(hits[i],p_orgin,dir.normalized,distance);

            //m_spellEffects.RootTransform(hits[i].GetComponent<Rigidbody>(), 3.0f);
        }
        ApplyTerrainEffect(this.transform.position, m_spellData.aoeData.m_radius / 10);
        //Debug.Log(hits.Length + "after refine");
    }
    private void ExplodeConditionals(Collider hitObject,Vector3 p_orgin, Vector3 explosionDir, float distance)
    {
        if (hitObject)
        {
            //Debug.Log(hitObject.transform.name);
            AIHealthScript hitHealth = hitObject.GetComponent<AIHealthScript>();
            scr_FlameSpreader temperatureScript = hitObject.GetComponent<scr_FlameSpreader>();
            Character t_character = hitObject.GetComponent<Character>();
            CharacterResourceManager resManager = hitObject.GetComponent<CharacterResourceManager>();
            if (t_character)
            {
                
                if (!m_spellData.isPlayerFriendly && t_character.GetIsPlayerFriendly() ||
               m_spellData.isPlayerFriendly && !t_character.GetIsPlayerFriendly())
                {
                    if (hitHealth != null)
                    {
                        hitHealth.TakeDamage(m_spellData.globalData.m_damage,false, explosionDir);
                    }
                    if (resManager != null)
                    {
                        resManager.DealDamage(m_spellData.globalData.m_damage);
                    }
                    if (temperatureScript == null)
                    {
                        temperatureScript = hitObject.gameObject.AddComponent<scr_FlameSpreader>();
                    }
                    if (temperatureScript != null)
                    {
                        temperatureScript.ModifyCelciusWithCalculatedCelcius(m_spellData.temperatureData.spellTemperature); // need to make temperature manager to add the scripts etc. 
                    }
                    if (m_spellData.knockBackData.m_afflictsKnockback)
                    {
                        //if (hitObject.GetComponent<NavMeshAgent>() != null)
                        //{
                        //    hitObject.GetComponent<NavMeshAgent>().Stop();
                        //    hitObject.GetComponent<Rigidbody>().isKinematic = false;
                        //}
                        if (m_spellData.knockBackData.m_knockbackDistanceFalloff == 0) //division by 0 otherwise 
                        {
                            m_spellData.m_spellEffects.ApplyDirectionalForce(hitObject.gameObject, explosionDir, m_spellData.knockBackData.m_knockbackForce, false);
                        }
                        else
                        {
                            m_spellData.m_spellEffects.ApplyDirectionalForce(hitObject.gameObject, explosionDir, m_spellData.knockBackData.m_knockbackForce / (distance * m_spellData.knockBackData.m_knockbackDistanceFalloff), false);
                        }
                    }
                    if (m_spellData.afflictionData.m_afflictsEffect)
                    {
                        AfflictionReciever t_reciever = hitObject.GetComponent<AfflictionReciever>();
                        if (t_reciever != null)
                        {
                            t_reciever.AddCurse(new Affliction(m_spellData.afflictionData.m_afflictionData));
                        }
                        else
                        {
                            //Debug.Log("Target that trying to affect with affliction doesnt have a reciever :(");
                        }
                    }
                }
            }
            else //if not character
            {
                if (hitHealth != null)
                {
                    hitHealth.TakeDamage(m_spellData.globalData.m_damage,false, explosionDir);
                }
                if (resManager != null)
                {
                    resManager.DealDamage(m_spellData.globalData.m_damage);
                }
                if (temperatureScript == null)
                {
                    temperatureScript = hitObject.gameObject.AddComponent<scr_FlameSpreader>();
                }
                if (temperatureScript != null)
                {
                    temperatureScript.ModifyCelciusWithCalculatedCelcius(m_spellData.temperatureData.spellTemperature); // need to make temperature manager to add the scripts etc. 
                }
                if (m_spellData.knockBackData.m_afflictsKnockback)
                {
                    //if (hitObject.GetComponent<NavMeshAgent>() != null)
                    //{
                    //    hitObject.GetComponent<NavMeshAgent>().Stop();
                    //    hitObject.GetComponent<Rigidbody>().isKinematic = false;
                    //}
                    if (m_spellData.knockBackData.m_knockbackDistanceFalloff == 0) //division by 0 otherwise 
                    {
                        m_spellData.m_spellEffects.ApplyDirectionalForce(hitObject.gameObject, explosionDir, m_spellData.knockBackData.m_knockbackForce, false);
                    }
                    else
                    {
                        m_spellData.m_spellEffects.ApplyDirectionalForce(hitObject.gameObject, explosionDir, m_spellData.knockBackData.m_knockbackForce / (distance * m_spellData.knockBackData.m_knockbackDistanceFalloff), false);
                    }
                }
                if (m_spellData.afflictionData.m_afflictsEffect)
                {
                    AfflictionReciever t_reciever = hitObject.GetComponent<AfflictionReciever>();
                    if (t_reciever != null)
                    {
                        t_reciever.AddCurse(new Affliction(m_spellData.afflictionData.m_afflictionData));
                    }
                    else
                    {
                        //Debug.Log("Target that trying to affect with affliction doesnt have a reciever :(");
                    }
                }
            }
        }
  
    }
    public void HomeTowardsPoint(Vector3 p_point)
    {
        m_rigid.velocity = this.transform.forward *m_spellData.projectileData.m_projectileSpeed;

        Quaternion targetRotation = Quaternion.LookRotation(m_closestTarget.transform.position-this.transform.position);
        m_rigid.MoveRotation(Quaternion.RotateTowards(this.transform.rotation, targetRotation, m_spellData.homingData.m_homingTurnRate)); //<--- turnspeed
    }
    public Collider FindandGetClosestTarget()
    {
        Collider[] inArea = GlobalRaycast.SphereCastForObjectsWithinRange(this.transform.position, m_spellData.aoeData.m_radius, m_spellData.layerData.m_affectedLayer);
        Collider[] inFOV = GlobalRaycast.RefineCollidersInExplosionFOV(inArea, this.transform.position, m_spellData.layerData.m_obstacleLayer);
        float distance = 999.0f;
        Collider closestTarget = null;
        for (uint i = 0; i < inFOV.Length; i++)
        {
            float t_distance = Vector3.Distance(this.transform.position, inFOV[i].transform.position);
            if (t_distance < distance)
            {
                Character targetCharacter = inFOV[i].GetComponent<Character>();
                if (targetCharacter)
                {
                    if (m_spellData.isPlayerFriendly != targetCharacter.GetIsPlayerFriendly())
                    {
                        distance = t_distance;
                        closestTarget = inFOV[i];
                    }
                }
            }
        }
        return closestTarget;
    }
    void Despawn()
    {
        //Debug.Log("Despawned this spell" + this.transform.name);
        Destroy(this.gameObject);
    }
    public void SetObjectPooler(ObjectPooler p_pooler)
    {
        objectPooler = p_pooler;
    }
    public GameObject CreateProjectileBase(ref GameObject p_projectileBase)
    {
        if (p_projectileBase == null)
        {
            p_projectileBase = Resources.Load("G/Prefabs/ProjectileBase") as GameObject;
        }
        if (p_projectileBase != null)
        {
            GameObject t_proj = Instantiate(p_projectileBase) as GameObject;
            return t_proj;
        }
        else
        {
            Debug.Log("Projectile base could not be found, spellcasting failed");
        }
        Debug.Log("Something went wrong when creating the projectileBase");
        return null;
    }
    void ApplyTerrainEffect(Vector3 pos, float p_radius)
    {
        RaycastHit hitInfo;
        LayerMask navModifiableLayer = 1 << LayerMask.NameToLayer("Nav_Modifiable");
        float spellTemp = m_spellData.temperatureData.spellTemperature;
        //freeze
        if (spellTemp < 0)
        {
            if (AffectTerrain.CheckForTerrainOnPoint(pos, out hitInfo, navModifiableLayer)) //checks for the terrain triggerbox
            {
                //Debug.Log(hitInfo.transform.name);
                AffectTerrain.ModifyTerrain(hitInfo.collider, pos, p_radius, DestructionShape.SHAPE_CIRCLE, 1, false);
            }
            else
            {
                //Debug.Log("Found nothing");
            }
        }
        else if (spellTemp > 100)
        {
            if (AffectTerrain.CheckForTerrainOnPoint(pos, out hitInfo, navModifiableLayer)) //checks for the terrain triggerbox
            {
                Debug.Log(hitInfo.transform.name);
                AffectTerrain.ModifyTerrain(hitInfo.collider, pos, p_radius, DestructionShape.SHAPE_CIRCLE, 0, true);
            }
            //else
            //{
            //    Debug.Log("Found nothing");
            //}
        }
        //melt
    }
}
