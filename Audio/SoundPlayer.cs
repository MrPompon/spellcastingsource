﻿using UnityEngine;
using System.Collections;

public class SoundPlayer : MonoBehaviour {

    public static void PlayRandomSoundAtPointFromArray(AudioClip[] audioClips,Vector3 p_position)
    {
        if (audioClips != null)
        {
        if (audioClips.Length > 0)
        {
          
                int rnd = Random.Range(0, audioClips.Length);
                AudioSource.PlayClipAtPoint(audioClips[rnd], p_position);
            }
            else
            {
                //Debug.Log("No audioclips are defined");
            }
        }
        else
        {
            //Debug.Log("AudioClips are null");
        }
    

    }
    public static void PlaySoundAtPoint(AudioClip audioClip, Vector3 p_position)
    {
        if (audioClip != null)
        {
            AudioSource.PlayClipAtPoint(audioClip, p_position);
        }

    }
}
