﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SpellBookSpell : MonoBehaviour {
    private Button uIButton;
    private Spell storedSpell;
    private SpellBook targetSpellBook;
    private CharacterAbilityController abilityController;
    public void SetStoredSpell(Spell p_spell)
    {
        storedSpell = p_spell;
    }
    public Spell GetStoredSpell()
    {
        return storedSpell;
    }
    public void SetButton(Button p_button)//DO THIS IMNPROTANT
    {
        uIButton = p_button;
    }
    public void SetButtonListenerToFunction(Button p_targetButton)
    {
        if (p_targetButton == null)
        {
            Debug.Log("UIButtonIsNull");
        }
        else
        {
            p_targetButton.onClick.AddListener(OverrideSpell);
        }
    }
    public void SetTargetSpellBook(SpellBook p_spellBook)
    {
        targetSpellBook = p_spellBook;
    }
    public void SetTargetAbilityController(CharacterAbilityController p_abilityController)
    {
        abilityController = p_abilityController;
    }
    void OverrideSpell()
    {
        abilityController.SetAbility(storedSpell);
        Debug.Log(storedSpell.m_spellData.m_name);
    }
}
