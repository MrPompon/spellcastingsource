﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
public class SpellCreationUIManager : MonoBehaviour
{
    SpellManaCostCalculator manaCostCalculator;
    [SerializeField]
   
    public ToggleGroup spellType;
    public ToggleGroup spellCastMethod;
    public Toggle afflicitonToggle;
    public Toggle spellBounceToggle;
    public Toggle homingToggle;
    public Toggle knockbackToggle;
    public Toggle pulseToggle;
    public Toggle channelToggle;
    public Transform afflictionTabPanel;
    public Transform bounceTabPanel;
    public Transform homingTabPanel;
    public Transform pulseTabPanel;
    public Transform knockbackTabPanel;
    public Transform channelTabPanel;
    public Transform currentSpellHolder;
    public Transform spellNameInput;
    public Transform spellDescInput;
    public InputField spellTemperatureInputField;
    CharacterResourceManager currentSpellHolderResources;
    public Transform spellBasePanel;
    public Text manaCostText;
    public SpellBook targetSpellBook;
    public SpellBookUIManager spellBookUIManager;
    public Button createSpellButton;
    AudioClip[] defaultSounds;
    AudioClip[] defaultCastSounds;
    public GameObject defaultExplosionEffect;
    // Use this for initialization
    void Start()
    {
        defaultSounds = Resources.LoadAll<AudioClip>("G/SFX/ExplosionSounds/Impact") as AudioClip[];
        defaultCastSounds = Resources.LoadAll<AudioClip>("G/SFX/ExplosionSounds/Cast") as AudioClip[];
        //defaultExplosionEffect= Resources.Load("G/Prefabs/SpellPrefabs/ExplosionEffect") as GameObject;
        manaCostCalculator = GameObject.FindGameObjectWithTag("SpellLibrary_Tag").GetComponent<SpellManaCostCalculator>();
        currentSpellHolder = GameObject.FindGameObjectWithTag("Player").transform;
        targetSpellBook = currentSpellHolder.GetComponent<SpellBook>();
        currentSpellHolderResources = currentSpellHolder.GetComponent<CharacterResourceManager>();
        createSpellButton.onClick.AddListener(GenerateSpell);
        createSpellButton.onClick.AddListener(spellBookUIManager.ManageSpellBookUI);
    
    }
    void TempSetSpellHolderSpellToGenererated(Transform spellHolder, ref Spell p_genSpell)
    {
        CharacterAbilityController tempAbilityController = spellHolder.GetComponent<CharacterAbilityController>();
        //tempAbilityController.OverwriteCurrentSpellAt(tempAbilityController.lH, p_genSpell);
    }
    public void GenerateSpell() //NOTE TO SELF, DONT USE A SINGLE DECLARED VARIABLE WHEN STORING IN LISTS ETC.
    {
        SpellBase.SpellData t_spellData = new SpellBase.SpellData(); 
         Spell generatedSpell=new Spell(t_spellData);
        UpdateTogglesFromGroups(ref generatedSpell);
        ManageUISpellBools(ref generatedSpell);
        HandleTabPanels(ref generatedSpell);
        HandleGeneralTab(ref generatedSpell, spellBasePanel);
        HandleSpellGeneralDescriptivePanel(ref generatedSpell);
        HandleTemperaturePanel(ref generatedSpell);
        SetDefaultSpellEffectAndSound(ref generatedSpell);
        CalculateSpellChannelInstances(ref generatedSpell);
        CalculateGenereratedSpellManaCost(ref generatedSpell);

        generatedSpell.m_spellData.isPlayerFriendly = true; //TEMPORARY if hit errors later check alignment on ai, player and the spell.
        targetSpellBook.AddSpellToList(generatedSpell);

        //TempSetSpellHolderSpellToGenererated(currentSpellHolder, ref generatedSpell);
  
    }
    void CalculateSpellChannelInstances(ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.channelingData.m_channelInstances = (generatedSpell.m_spellData.channelingData.m_channelDuration / generatedSpell.m_spellData.channelingData.m_channelCastRate);
    }
    void SetDefaultSpellEffectAndSound(ref Spell generatedSpell)
    {
        SetDefaultSound(ref generatedSpell);
        SetDefaultEffect(ref generatedSpell);
    }
    void SetDefaultSound(ref Spell generatedSpell)
    {
        generatedSpell.audioData.m_spellTriggerSounds = defaultSounds;
        generatedSpell.audioData.m_castSounds = defaultCastSounds;
    }
    void SetDefaultEffect(ref Spell generatedSpell)
    {
        if (generatedSpell.explosionEffect == null)
        {
            generatedSpell.explosionEffect = defaultExplosionEffect;
            Debug.Log(defaultExplosionEffect.name);
        }
    }
    public void UpdateTogglesFromGroups(ref Spell generatedSpell)
    {
        Toggle tempSpellType = ToggleGroupExtension.GetActive(spellType);
        ConvertUIToggleToSpellFunction(tempSpellType, ref generatedSpell);

        Toggle tempSpellCastMethod = ToggleGroupExtension.GetActive(spellCastMethod);
        ConvertUIToggleToSpellFunction(tempSpellCastMethod, ref generatedSpell);
    }
    void ConvertUIToggleToSpellFunction(Toggle activeToggle, ref Spell targetSpell)
    {
        switch (activeToggle.name)
        {
            case "TypeProjectile":
                targetSpell.m_spellData.m_typeProjectile = SpellTypeEnums.SpellTypeProjectile.TYPEPROJECTILE_BALL;
                break;
            case "TypeAOE":
                targetSpell.m_spellData.m_typeProjectile = SpellTypeEnums.SpellTypeProjectile.TYPEPROJECTILE_AOE;
                break;
            case "TypeRay":
                targetSpell.m_spellData.m_typeProjectile = SpellTypeEnums.SpellTypeProjectile.TYPEPROJECTILE_RAY;
                break;
            case "MethodSingle":
                targetSpell.m_spellData.m_typeAffected = SpellTypeEnums.SpellTypeAffected.TYPETARGET_SINGLE;
                break;
            case "MethodMultiple":
                targetSpell.m_spellData.m_typeAffected = SpellTypeEnums.SpellTypeAffected.TYPETARGET_MULTIPLE;
                break;
            case "MethodAOE":
                targetSpell.m_spellData.m_typeAffected = SpellTypeEnums.SpellTypeAffected.TYPETARGET_AOE;
                break;
        }
    }
    void ManageUISpellBools(ref Spell p_targetSpell)
    {
        HandleAfflictionToggle(ref p_targetSpell, afflicitonToggle);
        HandleBounceToggle(ref p_targetSpell, spellBounceToggle);
        HandleChannelCastToggle(ref p_targetSpell, channelToggle);
        HandleKnockbackToggle(ref p_targetSpell, knockbackToggle);
        HandleIsPulsingToggle(ref p_targetSpell, pulseToggle);
        HandleHomingToggle(ref p_targetSpell, homingToggle);
    }
    void HandleAfflictionToggle(ref Spell p_targetspell, Toggle p_afflictionToggle)
    {
        if (p_afflictionToggle.isOn)
        {
            p_targetspell.m_spellData.afflictionData.m_afflictsEffect = true;
        }
    }
    void HandleHomingToggle(ref Spell p_targetSpell, Toggle p_homingToggle)
    {
        p_targetSpell.m_spellData.homingData.m_isHoming = p_homingToggle.isOn;
    }
    void HandleBounceToggle(ref Spell p_targetspell, Toggle p_bounceToggle)
    {
        if (p_bounceToggle.isOn)
        {
            p_targetspell.m_spellData.m_spellCollisionBehavior = SpellTypeEnums.SpellProjectileCollisionBehaviour.COLLISION_BOUNCE;
        }
    }
    void HandleChannelCastToggle(ref Spell p_targetspell, Toggle p_channelToggle)
    {
        p_targetspell.m_spellData.channelingData.m_isChannelCast = p_channelToggle.isOn;
    }
    void HandleKnockbackToggle(ref Spell p_targetspell, Toggle p_knockbackToggle)
    {
        p_targetspell.m_spellData.knockBackData.m_afflictsKnockback = p_knockbackToggle.isOn;
    }
    void HandleIsPulsingToggle(ref Spell p_targetspell, Toggle p_pulsingToggle)
    {
        p_targetspell.m_spellData.pulseData.m_IsTriggeredWithInterval = p_pulsingToggle.isOn;
    }
    void HandleTemperaturePanel(ref Spell p_targetSpell)
    {
        p_targetSpell.m_spellData.temperatureData.spellTemperature =
            GetFloat(spellTemperatureInputField.text,0);
        //GetFloat(GetInputDataFromInputFieldViaTwoTransforms(spellBasePanel, "SpellTemperaturePanel", "InputField"), 0);
        Debug.Log(p_targetSpell.m_spellData.temperatureData.spellTemperature);
    }
    void HandleTabPanels(ref Spell p_targetSpell)
    {
        HandleAfflictionResTabPanel(ref p_targetSpell, afflictionTabPanel, "AfflictionResPanel", ref p_targetSpell);
        HandleAfflictionHealthTabPanel(ref p_targetSpell, afflictionTabPanel, "AfflictionHealthPanel", ref p_targetSpell);
        HandleAfflictionManaTabPanel(ref p_targetSpell, afflictionTabPanel, "AfflictionManaPanel", ref p_targetSpell);
        HandleAfflictionArmorTabPanel(ref p_targetSpell, afflictionTabPanel, "AfflictionArmorPanel", ref p_targetSpell);
        HandleAfflictionGeneralPanel(ref p_targetSpell, afflictionTabPanel, "AfflictionGeneralPanel", ref p_targetSpell);

        HandleBounceTabPanel(ref p_targetSpell, bounceTabPanel, "BounceSubPanel", ref p_targetSpell);

        HandleHomingTabPanel(ref p_targetSpell, homingTabPanel, "HomingSubPanel", ref p_targetSpell);

        HandlePulseTabPanel(ref p_targetSpell, pulseTabPanel, "PulseSubPanel", ref p_targetSpell);

        HandleKnockbackTabPanel(ref p_targetSpell, knockbackTabPanel, "KnockbackSubPanel", ref p_targetSpell);

        HandleChannelTabPanel(ref p_targetSpell, channelTabPanel, "ChannelSubPanel", ref p_targetSpell);
    }

    void HandleAfflictionResTabPanel(ref Spell p_targetSpell, Transform p_afflictionPanel, string p_afflictionResSubPanel, ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.afflictionData.m_afflictionData.resistanceMods.sm_lightningResistanceMod =
            GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionResSubPanel, "LightningResInput"), 0.691337f);

        generatedSpell.m_spellData.afflictionData.m_afflictionData.resistanceMods.sm_fireResistanceMod =
          GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionResSubPanel, "FireResInput"), 0.691337f);

        generatedSpell.m_spellData.afflictionData.m_afflictionData.resistanceMods.sm_iceResistanceMod =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionResSubPanel, "ColdResInput"), 0.691337f);

        generatedSpell.m_spellData.afflictionData.m_afflictionData.resistanceMods.sm_waterResistanceMod =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionResSubPanel, "WaterResInput"), 0.691337f);

        generatedSpell.m_spellData.afflictionData.m_afflictionData.resistanceMods.sm_venomResistanceMod =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionResSubPanel, "VenomResInput"), 0.691337f);

        generatedSpell.m_spellData.afflictionData.m_afflictionData.resistanceMods.sm_darknessResistanceMod =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionResSubPanel, "DarknessResInput"), 0.691337f);

        generatedSpell.m_spellData.afflictionData.m_afflictionData.resistanceMods.sm_holyResistanceMod =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionResSubPanel, "HolyResInput"), 0.691337f);
    }

    void HandleAfflictionHealthTabPanel(ref Spell p_targetSpell, Transform p_afflictionPanel, string p_afflictionHealthSubPanel, ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.afflictionData.m_afflictionData.healthMods.sm_hpMaxMod =
           GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionHealthSubPanel, "HealthMaxInput"), 0.691337f);

        generatedSpell.m_spellData.afflictionData.m_afflictionData.healthMods.sm_hpRegMod =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionHealthSubPanel, "HealthRegInput"), 0.691337f);
    }

    void HandleAfflictionManaTabPanel(ref Spell p_targetSpell, Transform p_afflictionPanel, string p_afflictionManaSubPanel, ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.afflictionData.m_afflictionData.manaMods.sm_manaRegMod =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionManaSubPanel, "ManaRegInput"), 0.691337f);

        generatedSpell.m_spellData.afflictionData.m_afflictionData.manaMods.sm_manaMaxMod =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionManaSubPanel, "ManaMaxInput"), 0.691337f);
    }
    void HandleAfflictionArmorTabPanel(ref Spell p_targetSpell, Transform p_afflictionPanel, string p_afflictionArmorSubPanel, ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.afflictionData.m_afflictionData.armorMods.sm_armorMod =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionArmorSubPanel, "ArmorInput"), 0.691337f);

        generatedSpell.m_spellData.afflictionData.m_afflictionData.armorMods.sm_armorMultiplier =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionArmorSubPanel, "ArmorMultiplier"), 0.691337f);
    }
    void HandleAfflictionGeneralPanel(ref Spell p_targetSpell, Transform p_afflictionPanel, string p_afflictionGeneralSubPanel, ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.afflictionData.m_afflictionData.duration =
      GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_afflictionPanel, p_afflictionGeneralSubPanel, "AfflictionDurationInput"), 0.691337f);
    }

    void HandleBounceTabPanel(ref Spell p_targetSpell, Transform p_bouncePanel, string p_bounceSubPanel, ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.bounceData.m_bounceAmount =
     (int)(GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_bouncePanel, p_bounceSubPanel, "BounceAmountInput"), 0.691337f));

        generatedSpell.m_spellData.bounceData.m_triggerEffectOnBounce =
      GetToggleBoxDataFromInputViaTwoTransforms(bounceTabPanel, p_bounceSubPanel, "TriggerEffectOnBounceToggle");
    }

    void HandleHomingTabPanel(ref Spell p_targetSpell, Transform p_homingPanel, string p_homingSubPanel, ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.homingData.m_homingTurnRate =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_homingPanel, p_homingSubPanel, "HomingTurnRateInput"), 0.691337f);
    }

    void HandlePulseTabPanel(ref Spell p_targetSpell, Transform p_pulsePanel, string p_pulseSubPanel, ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.pulseData.m_triggerIntervalRate =
        GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_pulsePanel, p_pulseSubPanel, "PulseRateInput"), 0.691337f);
    }

    void HandleKnockbackTabPanel(ref Spell p_targetSpell, Transform p_knockbackPanel, string p_knockbackSubPanel,ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.knockBackData.m_knockbackForce =
       GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_knockbackPanel, p_knockbackSubPanel, "KnockBackInput"), 0.691337f);

        generatedSpell.m_spellData.knockBackData.m_knockbackDistanceFalloff =
      GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_knockbackPanel, p_knockbackSubPanel, "KnockBackFalloffInput"), 0.691337f);
    }

    void HandleChannelTabPanel(ref Spell p_targetSpell, Transform p_channelPanel, string p_channelSubPanel,ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.channelingData.m_channelCastRate =
    GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_channelPanel, p_channelSubPanel, "ChannelCastRateInput"), 0.691337f);
        if (generatedSpell.m_spellData.channelingData.m_channelCastRate == 0)
        {
            generatedSpell.m_spellData.channelingData.m_channelCastRate = 0.01f;
        }

        generatedSpell.m_spellData.channelingData.m_canMoveWhileChanneling =
   GetToggleBoxDataFromInputViaTwoTransforms(p_channelPanel, p_channelSubPanel, "CanMoveWhileChanneling");

    }
    void HandleGeneralTab(ref Spell p_targetSpell, Transform p_generalPanel)
    {
        HandleGeneralGlobalData(ref p_targetSpell, p_generalPanel, "GlobalData",ref p_targetSpell);
        HandleGeneralProjectileData(ref p_targetSpell, p_generalPanel, "ProjectileData", ref p_targetSpell);
    }
    void HandleGeneralGlobalData(ref Spell p_targetSpell, Transform p_generalPanel, string p_globalDataSubPanel, ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.globalData.m_spawnAmount =
    (int)(GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_generalPanel, p_globalDataSubPanel, "SpawnAmountInput"), 0.691337f));

        generatedSpell.m_spellData.globalData.m_range =
       GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_generalPanel, p_globalDataSubPanel, "SpellRangeInput"), 0.691337f);

        generatedSpell.m_spellData.globalData.m_damage =
     GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_generalPanel, p_globalDataSubPanel, "SpellDamageInput"), 0.691337f);

        generatedSpell.m_spellData.channelingData.m_channelDuration =
     GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_generalPanel, p_globalDataSubPanel, "SpellCastTimeInput"), 0.691337f);

        generatedSpell.m_spellData.aoeData.m_radius =
     GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_generalPanel, p_globalDataSubPanel, "SpellCastAOEInput"), 0.691337f);
    }

    void HandleGeneralProjectileData(ref Spell p_targetSpell, Transform p_generalPanel, string p_projectileDataSubPanel, ref Spell generatedSpell)
    {
        generatedSpell.m_spellData.projectileData.m_duration =
    GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_generalPanel, p_projectileDataSubPanel, "ProjectileDurationInput"), 0.691337f);

        generatedSpell.m_spellData.projectileData.m_projectileSpeed =
    GetFloat(GetInputDataFromInputFieldViaTwoTransforms(p_generalPanel, p_projectileDataSubPanel, "ProjectileSpeedInput"), 0.691337f);
    }
    void HandleSpellGeneralDescriptivePanel(ref Spell p_targetSpell)
    {
        //temporaryRageFix
        p_targetSpell.m_spellData.m_name =
            spellNameInput.Find("Text").GetComponent<Text>().text;
        p_targetSpell.m_spellData.m_description =
            spellDescInput.Find("Text").GetComponent<Text>().text;
    }
    void CalculateGenereratedSpellManaCost(ref Spell generatedSpell)
    {
        if (currentSpellHolderResources == null)
        {
            Debug.Log("Cannot find the target SpellHolder In The UIManager");
        }
        //currentSpellHolderResources.
        float tempManacost = manaCostCalculator.CalculateManaCost(generatedSpell.m_spellData);
        manaCostText.text = tempManacost.ToString();
        generatedSpell.m_spellData.calculatedManaCost = tempManacost;
    }

    private float GetFloat(string stringValue, float defaultValue)
    {
        float result = defaultValue;
        float.TryParse(stringValue, out result);
        return result;
    }

    private string GetInputDataFromInputFieldViaTwoTransforms(Transform p_parentTransform, string p_childTransformName, string p_inputFieldName)
    {
        Transform temp_child = p_parentTransform.Find(p_childTransformName);
        if (temp_child == null)
        {
            Debug.LogError("Could not find the transform(" + p_childTransformName + ") from the UIManager);");
            return null;
        }
        Transform tempInputfield = temp_child.Find(p_inputFieldName);
        if (tempInputfield == null)
        {
            Debug.LogError("Could not find the inputFieldsTransform(" + p_inputFieldName + ") from the UIManager);");
            return null;
        }
        return tempInputfield.Find("InputField").GetComponent<InputField>().text;
    }

    private bool GetToggleBoxDataFromInputViaTwoTransforms(Transform p_parentTransform, string p_childTransformName, string p_toggleBoxName)
    {
        Transform temp_child = p_parentTransform.Find(p_childTransformName);
        if (temp_child == null)
        {
            Debug.Log("Could not find the transform(" + p_childTransformName + ") from the UIManager);");
            return false;
        }
        Transform tempToggleBox = temp_child.Find(p_toggleBoxName);
        if (tempToggleBox == null)
        {
            Debug.Log("Could not find the inputFieldsTransform(" + p_toggleBoxName + ") from the UIManager);");
            return false;
        }
        return tempToggleBox.GetComponent<Toggle>().isOn;
    }
}
