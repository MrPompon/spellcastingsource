﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpellComboHandler : MonoBehaviour {

	// Use this for initialization
    private int currentCombo = 0;
    private float currentComboMultiplier = 0;
    float currentComboResetDelay = 1.0f;
    private float currentComboResetProgress;
    private bool currentlyCasting=false;
    GameObject abilityCastCanvas;
    Image comboDurationImage;
    float comboDurationBarWidth, comboDurationBarHeight;
    Image comboMinTimingImage, comboMaxTimingImage;
    public Text comboBarText;
    private int comboTextFontSize;
    private float comboTimingMin, comboTimingMax;
    int lowCombo, mediumCombo, highCombo, extremeCombo;
    void Awake()
    {
        lowCombo = 3;
        mediumCombo = 6;
        highCombo = 9;
        extremeCombo = 11;
        abilityCastCanvas = GameObject.FindGameObjectWithTag("CastingCanvas");
        comboDurationImage = abilityCastCanvas.transform.Find("Combo").transform.Find("Image").GetComponent<Image>();
        comboMinTimingImage = comboDurationImage.transform.Find("ImageMinTiming").GetComponent<Image>();
        comboMaxTimingImage = comboDurationImage.transform.Find("ImageMaxTiming").GetComponent<Image>();
        comboBarText = abilityCastCanvas.transform.Find("Combo").transform.Find("Text").GetComponent<Text>();
        comboTextFontSize = comboBarText.fontSize;
    }
    void Start()
    {
        ResetCombo();
        SetComboTiming(0.6f, 0.8f);
        SetComboTimingImagesVertical();
    }
    void LateUpdate()
    {
        if (currentCombo > 0 && !currentlyCasting)
        {
            HandleComboMeter();
            currentComboResetProgress += Time.deltaTime;
            if (currentComboResetProgress > currentComboResetDelay)
            {
                ResetCombo();
            }
        }
    }
    public void CheckComboAttempt(float p_timing)
    {
        if (currentCombo > 0)
        {
            if (IsComboTimingCorrect(p_timing))
            {
                IncreaseCombo(1);
            }
            else
            {
                ResetCombo();
            }
        }
        else
        {
            IncreaseCombo(1);
        }
    }
    public void IncreaseCombo(int p_increaseAmount)
    {
        currentCombo += p_increaseAmount;
        ResetComboResetProgress();
        UpdateComboBar();
    }
    public bool IsComboTimingCorrect(float p_timing)
    {
        if (p_timing >= comboTimingMin && p_timing <= comboTimingMax)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void ResetCombo()
    {
        currentCombo = 0;
        ResetComboResetProgress();
        UpdateComboBar();
        SetComboUIVisibilty(false);
    }
    public float GetCurrentComboMultiplier()
    {
        return currentComboMultiplier;
    }
    void ResetComboResetProgress()
    {
        currentComboResetProgress = 0;
    }
    public void SetIsCurrentlyCasting(bool p_trueOrFalse)
    {
        currentlyCasting = p_trueOrFalse;
    }

    void SetComboBarText()
    {
        comboBarText.text = "X"+currentCombo+"!";
    }
    void SetComboBarTextColor(Color p_color)
    {
        comboBarText.color = p_color;
    }
    void CalculateComboBarTextColor(int p_comboAmount)
    {
        if (p_comboAmount < lowCombo)
        {
            SetComboBarTextColor(Color.white);
        }
        else if (p_comboAmount > lowCombo && p_comboAmount < mediumCombo)
        {
            SetComboBarTextColor(Color.green);
        }
        else if (p_comboAmount > mediumCombo && p_comboAmount < highCombo)
        {
            SetComboBarTextColor(Color.yellow);
        }
        else if (p_comboAmount > highCombo && p_comboAmount < extremeCombo)
        {
            SetComboBarTextColor(Color.red);
        }
    }
    void ModifyComboHitRange(int p_comboAmount)
    {
        if (p_comboAmount < lowCombo)
        {
            SetComboTiming(0.5f, 0.9f);
        }
        else if (p_comboAmount > lowCombo && p_comboAmount < mediumCombo)
        {
            SetComboTiming(0.6f, 0.8f);
        }
        else if (p_comboAmount > mediumCombo && p_comboAmount < highCombo)
        {
            SetComboTiming(0.7f, 0.8f);
        }
        else if (p_comboAmount > highCombo && p_comboAmount < extremeCombo)
        {
            SetComboTiming(0.75f, 0.8f);
        }
    }
    void UpdateComboBar()
    {
        CalculateComboBonuses(currentCombo);
        SetComboUIVisibilty(true);

        SetComboBarText();
        CalculateComboBarTextColor(currentCombo);
        CalculateTextFontSize(currentCombo);

        ModifyComboHitRange(currentCombo);
        SetComboTimingImagesVertical();
    }
    void CalculateComboBonuses(int p_comboAmount)
    {//decreasecomboresetdelay
        if (p_comboAmount < lowCombo)
        {
            currentComboMultiplier = 1;
        }
        else if (p_comboAmount > lowCombo && p_comboAmount < mediumCombo)
        {
            currentComboMultiplier = 1.2f;
        }
        else if (p_comboAmount > mediumCombo && p_comboAmount < highCombo)
        {
            currentComboMultiplier = 1.6f;
        }
        else if (p_comboAmount > highCombo && p_comboAmount < extremeCombo)
        {
            currentComboMultiplier = 2f;
        }
    }
    void CalculateTextFontSize(int p_comboAmount)
    {
        if (p_comboAmount < lowCombo)
        {
            comboBarText.fontSize = comboTextFontSize;
        }
        else if (p_comboAmount > lowCombo && p_comboAmount < mediumCombo)
        {
            comboBarText.fontSize = comboTextFontSize + 6;
        }
        else if (p_comboAmount > mediumCombo && p_comboAmount < highCombo)
        {
            comboBarText.fontSize = comboTextFontSize + 12;
        }
        else if (p_comboAmount > highCombo && p_comboAmount < extremeCombo)
        {
            comboBarText.fontSize = comboTextFontSize + 18;
        }
    }
    void HandleComboMeter()
    {
        comboDurationImage.fillAmount = currentComboResetProgress/currentComboResetDelay;
    }
    void SetComboTiming(float p_min, float p_max)
    {
        SetComboTimingMin(p_min);
        SetComboTimingMax(p_max);
    }
    void SetComboTimingMin(float p_min)
    {
        comboTimingMin = p_min;
    }
    void SetComboTimingMax(float p_max)
    {
        comboTimingMax = p_max;
    }
    void SetComboTimingImages()
    {
        comboDurationBarWidth = comboDurationImage.rectTransform.rect.width;
        comboDurationBarHeight = comboDurationImage.rectTransform.rect.height;
        
        Vector2 comboBarPosXStart = new Vector2(comboDurationImage.rectTransform.position.x-comboDurationBarWidth / 2, comboDurationImage.rectTransform.position.y);
        Vector2 comboBarPosXEnd = new Vector2(comboDurationImage.rectTransform.position.x + comboDurationBarWidth / 2, comboDurationImage.rectTransform.position.y);

        comboMinTimingImage.rectTransform.position = Vector3.Lerp(comboBarPosXStart, comboBarPosXEnd, comboTimingMin);

        comboMaxTimingImage.rectTransform.position = Vector3.Lerp(comboBarPosXStart, comboBarPosXEnd, comboTimingMax);
    }
    void SetComboTimingImagesVertical()
    {
        comboDurationBarWidth = comboDurationImage.rectTransform.rect.width;
        comboDurationBarHeight = comboDurationImage.rectTransform.rect.height;

        //Vector2 comboBarPosXStart = new Vector2(comboDurationImage.rectTransform.position.x - comboDurationBarWidth / 2, comboDurationImage.rectTransform.position.y);
        //Vector2 comboBarPosXEnd = new Vector2(comboDurationImage.rectTransform.position.x + comboDurationBarWidth / 2, comboDurationImage.rectTransform.position.y);

        Vector2 comboBarPosYStart = new Vector2(comboDurationImage.rectTransform.position.x, comboDurationImage.rectTransform.position.y - comboDurationBarHeight / 2);
        Vector2 comboBarPosYEnd = new Vector2(comboDurationImage.rectTransform.position.x, comboDurationImage.rectTransform.position.y + comboDurationBarHeight / 2);
        comboMinTimingImage.rectTransform.position = Vector3.Lerp(comboBarPosYStart, comboBarPosYEnd, comboTimingMin);

        comboMaxTimingImage.rectTransform.position = Vector3.Lerp(comboBarPosYStart, comboBarPosYEnd, comboTimingMax);
    }
    void SetComboUIVisibilty(bool trueOrFalse)
    {
        comboBarText.enabled = trueOrFalse;
        comboDurationImage.enabled = trueOrFalse;
    }
    public float GetComboProgress()
    {
        return currentComboResetProgress;
    }
}
